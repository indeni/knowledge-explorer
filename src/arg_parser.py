from argparse import (ArgumentParser, RawTextHelpFormatter)


class ArgParser(ArgumentParser):
    def __init__(self):
        description = 'New build parameters.'
        super(ArgParser, self).__init__(description=description, formatter_class=RawTextHelpFormatter)

    def parse_args(self):

        self.add_argument('--full-documentation', '-d',
                          action='store',
                          dest='full_documentation',
                          default=False,
                          #type=bool,
                          help='create knowledge base full documentation: download latest knowledge and generate HTML documentation from RST')

        self.add_argument('--knowledge-db', '-k',
                          action='store',
                          dest='knowledge_db',
                          default=False,
                          #type=bool,
                          help='create knowledge db file in a json format')

        self.add_argument('--knowledge-db-from-local', '-l',
                          action='store',
                          dest='knowledge_db_from_local',
                          default=False,
                          #type=bool,
                          help='create knowledge db file in a json format')

        self.add_argument('--knowledge-branch', '-b',
                          action='store',
                          dest='knowledge_branch',
                          default='master',
                          type=str,
                          help='Specify the knowledge branch to use')


        self.add_argument('--build-version', '-v',
                          action='store',
                          dest='build_version',
                          default='0.0.0',
                          type=str,
                          help='Specify the knowledge version')

        self.add_argument('--knowledge-dir-path', '-p',
                          action='store',
                          dest='knowledge_dir_path',
                          default='',
                          type=str,
                          help='Specify the knowledge dir path')


        return super(ArgParser, self).parse_args()