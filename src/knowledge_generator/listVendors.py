import csv
import os


# Helper method to see if the previous vendor matches the current vendor
# If vendors match, the vendor should not be written into the Valid Vendor file
# Takes: String, String; Returns: Boolean
def prevSeen(vendorPrev, vendorHere):
    if vendorPrev == vendorHere:
        return False

    return True

# Helper method to check if current category exists in array of categories already recorded
# Takes: String, Array; Returns: Boolean
# ## Note: To be implemented when all Indeni rules/scripts can be categorized
def alreadyOnCatFile(category, catArr):
    for i in catArr:
        if category == i:
            return False

    return True

# Helper method that writes category to category file if it is not already on file
# Takes: Array, String, file to write in; Returns: Nothing, writes in file
# ## Note: To be implemented when all Indeni rules/scripts can be categorized
def catFileHelper(catArr, category, f):
    if (alreadyOnCatFile(category, catArr)):
        if len(category) > 1:
            f.write(category.title())
            f.write(",")
            catArr.append(category)

# Main method that creates CSV file to list all vendors that Indeni has relations with
# Currently, all knowledge is organized by Vendor
def listVendors(knowledgeDBFile):

  with open(os.path.expanduser(knowledgeDBFile)) as csvfile:

        knowledgeParser = csv.reader(csvfile, delimiter=',')

        firstLine = True
        previousVendor = ""
        f = open("validVendorList.csv", "w+")
        for row in knowledgeParser:
           if firstLine:
                firstLine = False
                continue

           if(prevSeen(previousVendor, row[9])):
                previousVendor = row[9]
                f.write(row[9])
                f.write(",")

        f.close()