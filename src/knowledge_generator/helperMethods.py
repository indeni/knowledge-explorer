import os
import csv
import json
import re

# Helper method for writing new lines in the file
# Takes: File to write in; Returns: Nothing, writes data in file
def newLine(f):
    f.write("\n")

# Helper method to add the number of spaces required by Sphinx, in this case, 4 spaces
# Takes: Nothing; Returns: String
def addSpaces():
    return "    "

# Helper method to call newLine twice, to add extra space to the RST file for different sections
# Takes: File to write in; Returns: Nothing, writes in file
def doubleNewLine(f):
    newLine(f)
    newLine(f)

# Helper method to write a line of "=" as long as, or longer than, the header text
# Takes: String; Returns: String
def helperFillsEquals(headerText, headlineCharacter = "="):
    fullLine = ""
    for i in range(len(headerText)):
        fullLine += headlineCharacter
    return fullLine

# Simple helper method to check if OS Name is "FALSE"
# Takes: String; Returns: Boolean
def osNameFalse(osName):
    return osName.lower() == "false"

# Helper method to add the -Osname, or add nothing to the string if no os (like for F5 Scripts)
# Takes: String; Returns: String
def addOsOrNothing(os):
    if not (osNameFalse(os)):
        return "-" + osHandle(os)


    return ""

# Prints Multi-OS in page title instead of all the names of OS's supported
# Takes: String; Returns: String
def osHandle(osInput):
    if "," in osInput:
        return "Multi-OS"
    else:
        return osInput

# Helper methods to serve OS whose names are not simple lowercase
# Takes: String; Returns: String
def serveOS(osName):
    if osName.lower() == "ipso":
        return "IPSO"
    elif osName.lower() == "gaia":
        return "GAiA"
    elif osName.lower() == "secureplatform":
        return "SecurePlatform"
    elif osName.lower() == "ios":
        return "iOS"
    elif osName.lower() == "sgos":
        return "SGOS"
    elif osName.lower() == "nxos":
        return "NX-OS"
    elif osName.lower() == "panos":
        return "PAN-OS"
    elif osName.lower() == "alteon-os":
        return "Alteon-OS"
    elif osName.lower() == "gaia-embedded":
        return "GAiA-Embedded"
    elif osName.lower() == "rhel":
        return "RHEL"
    elif osName.lower() == "bam":
        return "BAM"
    elif osName.lower() == "bdds":
        return "BDDS"
    else:
        return osName.title()

# Helper method to space out and capitalize Palo Alto Networks
# Takes: String; Returns: String
def serveVendor(vendorName):
    if vendorName.lower() == "paloaltonetworks":
        return "Palo Alto Networks"
    elif vendorName.lower() == "checkpoint":
        return "Check Point"
    elif vendorName.lower() == "bluecoat":
        return "Blue Coat"
    elif vendorName.lower() == "zscaler":
        return "Zscaler"
    elif vendorName.lower() == "bluecat":
        return "BlueCat"
    else :
        return vendorName

# Method for creating the file name, based on the rule name and vendor name
# File created must always be a .rst file
# Note: different ways based on whether file is homepage, filterpage, or regular documentation file
# Takes: String, String, Boolean, Boolean; Returns: String, which will be the name of this RST file
def createFileName(name, vendorName, osName, isHomePage, isFilterPage, rstFolder):

    toBeFileName = ""

    if isHomePage:
        toBeFileName = name

    elif isFilterPage:
        toBeFileName = vendorName

    else:
        if not (osNameFalse(osName)):
            toBeFileName = name + "-" + vendorName + "-" + osName
        else :
            toBeFileName = name + "-" + vendorName

    if len(rstFolder) > 0:
        x = open(rstFolder + '/' + toBeFileName + ".rst", "a+")
        if os.stat(rstFolder + '/' + toBeFileName + ".rst").st_size == 0:
            x.close()
            return toBeFileName + ".rst"
        else:
            x.close()
            return createFileOffshoot(toBeFileName, 1, rstFolder)
    else:
        x = open(toBeFileName + ".rst", "a+")
        if os.stat(toBeFileName + ".rst").st_size == 0:
            x.close()
            return toBeFileName + ".rst"
        else:
            x.close()
            return createFileOffshoot(toBeFileName, 1, rstFolder)

# Helper method to format multiple OS's in a user-friendly way
# Takes: String; Returns: String
def formatMultipleOS(rowData):
    toReturn = ""
    listOfOS = rowData.split(",")
    for name in listOfOS:
        if(len(toReturn) < 1):
            toReturn += serveOS(name)
        else :
            toReturn += ", "
            toReturn += serveOS(name)

    return toReturn

# Helper method to get data stored under given key
# Takes: String, String, Int; Returns: String
def findToWrite(key, data, removeFromEnd):
    return data[data.find(key) + len(key) + 4:data.find('",:')-removeFromEnd]

# Helper method to style category names by adding underscores
# Takes: String; Returns: String
# ## Note: To be implemented when all Indeni rules/scripts can be categorized
def catStyler(catName):
    return catName.replace(" ", "_")

# Method to build link to rule file, by making the base of the URL and then using a helper method to find the rest
# Takes: String; Returns: String
def makeRuleLink(fileName):
    return "\"https://bitbucket.org/indeni/indeni-knowledge/src/staging/" + findPathFromJSON(fileName) + "\""

# Helper method to append strings under "description" that have "+" in them
# Displays as one string instead of several strings split up by +
# Takes: String; Returns: String
def handlePlus(str):
    str = str.replace('\n', '').replace('\r', '')
    stringTotal = ""
    splitStr = str.split('+')
    for part in splitStr:
        splitPart = part.split('"')
        for data in splitPart:
            if any(c.isalpha() for c in data):
                stringTotal += data
    return stringTotal

# Helper method to create links from Knowledge Explorer to Rule source code
# Takes file name, parses RuleToRulePath txt file, in JSON form, to build the link to the Rule source code
# Takes: String; Returns: String
def findPathFromJSON(fileName):
    with open(os.path.expanduser('./ruleIdToRulePath.txt'), 'r') as ruleJSON:
        lineByLine = ruleJSON.read()
        lineSplit = lineByLine.split(",")
        for line in lineSplit:
            if fileName in line:
                stringPath = line.split(":")[1:]
                return stringPath[0][1:-1]

    return ""

# Helper method to handle organizing files and links by Category
# Must be able to place files into 0, 1, or multiple categories
# Takes: String, String; Returns: Nothing, writes in given Category Filter Page files
# ## Note: To be implemented when all Indeni rules/scripts can be categorized
def handleCat(category, title):
    if (len(category) < 2):
       f = open("Non-Categorized.rst", "a+")
       f.write(addSpaces() + title)
       newLine(f)
       f.close()
    else :
       if "," in category:
           y = [x.strip() for x in category.split(',')]
           for i in y:
                handleCat(i, title)

       else :
            f = open(catStyler(category.title()) + ".rst", "a+")

            f.write(addSpaces() + title)
            newLine(f)
            f.close()