import os
import csv
import sys
from .helperMethods import newLine, addSpaces, doubleNewLine, helperFillsEquals, addOsOrNothing, serveVendor, \
    formatMultipleOS
from .helperMethods import osNameFalse, osHandle, createFileName, findToWrite, catStyler, handleCat, serveOS
import shutil

# Set recursion limit higher to handle recursing several times through over 600 JSON objects
sys.setrecursionlimit(2000)


# Method for creating headlines in the RST file, differently bolded
# depending on whether the headline is the main one or not
# Takes: String, Boolean, File to write in; Returns: Nothing, writes data in file
def helperFillHeadlineRST(headerText, mainHeader, f, headlineCharacter="*"):
    if mainHeader:
        f.write(helperFillsEquals(headerText, headlineCharacter))

    newLine(f)
    f.write(headerText)
    newLine(f)
    f.write(helperFillsEquals(headerText, headlineCharacter))
    newLine(f)


# Makes an RST block with headline and data to add under the headline
# Takes: String, String, File to write in; Returns: Nothing, writes in file
def makePart(heading, dataToWrite, f):
    helperFillHeadlineRST(heading, False, f, '^')
    f.write(addSpaces() + dataToWrite)
    doubleNewLine(f)


# Makes the Rule Link substitution, allowing the user to open the link in another tab
# Takes: File to write in, String; Returns: Nothing, writes in file
def makeRuleHTML(f, link):
    f.write(".. |rule_code| raw:: html")
    newLine(f)
    newLine(f)
    f.write(addSpaces() + '<a href="')
    f.write(link)
    f.write('" target=')
    f.write('"%s"' % "_blank")
    f.write(">See Rule Code</a>")
    doubleNewLine(f)


# Makes the IND Link substitution, allowing the user to open the link in another tab
# Takes: File to write in, String, String; Returns: Nothing, writes in file
def makeIndHTML(f, link, subName):
    f.write(".. |" + subName + "| raw:: html")
    newLine(f)
    newLine(f)
    f.write(addSpaces() + "<a href=")
    f.write('"%s"' % link)
    f.write(" target=")
    f.write('"%s"' % "_blank")
    f.write(">See Script Code</a>")
    doubleNewLine(f)


# Makes table for further reading that contains IND link
# Takes: File to write in, String, Int; Returns: Nothing, writes in file
def makeIndTable(f, linkName, linkNum):
    f.write(addSpaces() + "* - Link to Script " + linkName)
    newLine(f)
    f.write(addSpaces() + "  - |indURL_" + str(linkNum) + "|")
    newLine(f)


# Helper method to write the base of the Link Table
# Takes: File to write in; Returns: Nothing, writes in file
def makeTableBase(f):
    f.write(".. list-table::")
    newLine(f)
    f.write(addSpaces() + ":widths: 50 50")
    newLine(f)
    f.write(addSpaces() + ":header-rows: 1")
    newLine(f)
    f.write(addSpaces() + ":align: center")
    doubleNewLine(f)
    f.write(addSpaces() + "* - Link Description")
    newLine(f)
    f.write(addSpaces() + "  - Link")
    newLine(f)


# Displays table with link to Rule source code in bitbucket, if source code can be found
# Takes: File to write in; Returns: Nothing, writes in file
def makeRuleTable(f):
    f.write(addSpaces() + "* - Link to Rule Code")
    newLine(f)
    f.write(addSpaces() + "  - |rule_code|")
    newLine(f)


# Makes the home page by calling helper methods to generate the list of vendors to display
# Takes: File to write in; Returns: Nothing, writes in file
def makeHomePage(f, rstFolder):
    helperFillHeadlineRST("Indeni Knowledge Explorer App", True, f)
    helperFillHeadlineRST("Choose an Issue by Vendor", False, f)
    generateVendorList(f, rstFolder)
    # helperFillHeadlineRST("Choose an Issue by Category", False, f)
    # generateCategoryList(f)


# Generates all vendors for whom Indeni has rules/scripts, by pulling their names from the CSV file,
# and displays them on the home page to make it easier to search for rule documentation by Vendor
# Takes: File to write in, returns: Nothing, writes in file
def generateVendorList(f, rstFolder):
    with open("validVendorList.csv", 'r') as vendorsCSV:
        vendors = csv.reader(vendorsCSV, delimiter=',')
        for line in vendors:
            for i in range(len(line)):
                if len(line[i]) > 0:
                    g = open(createFileName("", rstFolder + "/" + line[i], "", False, True, ""), "a+")
                    helperMakeVendorSearch(g, line[i])
                    makeSearchPageByVendor(f, line[i], 1)
                    f.write(addSpaces() + line[i])
                    newLine(f)
                    g.close()


# Method to generate list of all possible categories, and generic category for non-categorized rules
# Takes: File to write in; Returns: Nothing, writes in file
def generateCategoryList(f):
    with open("categoryList.csv", 'r') as catsCSV:
        categories = csv.reader(catsCSV, delimiter=',')
        for line in categories:
            for i in range(len(line)):
                if len(line[i]) > 0:
                    g = open(createFileName("", catStyler(line[i]), "", False, True, ""), "a+")
                    helperMakeCatSearch(g, line[i])
                    makeSearchPageByCategory(f, line[i], 1)
                    f.write(addSpaces() + catStyler(line[i]))
                    newLine(f)
                    g.close()


# Helper method to generate the Vendor filter pages, making the title and the toctree of the page
# Takes: File to write in, String; Returns: Nothing, writes in file
def helperMakeVendorSearch(f, vendorName):
    helperFillHeadlineRST(serveVendor(vendorName.capitalize()) + " Issues", True, f)
    makeSearchPageByVendor(f, vendorName.capitalize(), -1)


# Helper method to generate the Category filter pages, making the title and the toctree of the page
# Takes: File to write in, String; Returns: Nothing, writes in file
def helperMakeCatSearch(f, category):
    helperFillHeadlineRST(category + " Issues", True, f)
    makeSearchPageByCategory(f, category, -1)


# Makes a toctree in the RST to be organized by Category
# Takes: File to write in, String, Int; Returns: Nothing, writes in file
def makeSearchPageByCategory(f, category, maxDepth):
    newLine(f)
    f.write(".. toctree::")
    newLine(f)
    f.write(addSpaces() + ":maxdepth: " + str(maxDepth))
    newLine(f)
    f.write(addSpaces() + ":titlesonly:")
    newLine(f)
    f.write(addSpaces() + ":caption: Category: " + category + " Issues")
    doubleNewLine(f)


# Makes a toctree in the RST to be organized by Vendor
# Takes: File to write in, String, Int; Returns: Nothing, writes in file
def makeSearchPageByVendor(f, vendorName, maxDepth):
    newLine(f)
    f.write(".. toctree::")
    newLine(f)
    f.write(addSpaces() + ":maxdepth: " + str(maxDepth))
    newLine(f)
    f.write(addSpaces() + ":titlesonly:")
    doubleNewLine(f)
    # f.write(addSpaces() + ":caption: Vendor: " + serveVendor(vendorName.capitalize()) + " Issues")
    # doubleNewLine(f)


# Helper method to parse JSON file and find all lines data stored in this JSON object
# Takes: Array, String; Returns: Nothing, writes in file
def parseLines(linesArray, rst_folder):
    try:
        relevantLines = []

        for line in linesArray:
            if "::" in line:
                titleBase = line[line.find('"') + 1:]
                finalTitle = titleBase[:titleBase.find("::") - 1]
                f = open(rst_folder + "/" + finalTitle + ".rst", "a+")
                findImportantLines(relevantLines, linesArray[1:], f, rst_folder, finalTitle)

    except Exception:
        pass


# Helper method to find all lines relevant to this JSON object
# Takes: Array, Array, File to write in, String, String; Returns: Nothing, writes in file
def findImportantLines(relevant, allLines, f, rst_folder, title):
    theseLines = relevant
    counter = 0
    for line in allLines:
        counter += 1
        if ");," in line:
            writeFileWithLines(theseLines, allLines[counter:], f, rst_folder, title)
        else:
            theseLines.append(line)


# Helper method to find all relevant data in these lines and write them to given RST file
# Continues recursion, so all files get accurately generated
# Takes: Array, Array, File to write in, String, String; Returns: Nothing, writes in file
def writeFileWithLines(linesToWrite, restLines, f, rst_folder, finalTitle):
    # Count how many IND links are put in file
    metLinkCount = 0
    ruleLink = ""
    indNameArr = []

    for line in linesToWrite:

        # Write to file depending on what is found

        if '"vendor":' in line:
            ven = findToWrite("vendor", line, 0)
            venName = serveVendor(ven)
            makePart("Vendor", venName.title(), f)
            h = open(rst_folder + '/' + ven + ".rst", "a+")
            h.write(addSpaces() + finalTitle)
            newLine(h)
            h.close()
        if '"OS":' in line:
            rawOS = findToWrite("OS", line, 0)
            if "," in rawOS:
                makePart("OS", formatMultipleOS(rawOS), f)
            else:
                makePart("OS", serveOS(rawOS), f)
        if "alertHeadline" in line:
            helperFillHeadlineRST(findToWrite("alertHeadline", line, 0), True, f)
            helperFillHeadlineRST("Rule details", False, f, "#")
        if "alertDescription" in line:
            writeHelp("Description", "alertDescription", line, f)
        if "alertRemediation" in line:
            writeHelp("General Remediation", "alertRemediation", line, f)
        if "ruleName" in line:
            writeHelp("Name of Rule", "ruleName", line, f)
        if "ruleFile" in line:
            ruleLink = findToWrite("ruleFile", line, 3)
            makeRuleHTML(f, ruleLink)

        if "scriptName" in line:
            indNameArr.append(findToWrite("scriptName", line, 0))
        if "metricName" in line:
            helperFillHeadlineRST('Metric ' + str(metLinkCount + 1) + ': "' + findToWrite("metricName", line, 0) + '"', False, f, "#")
        if '"how":' in line:
            writeHelp("How does this work? ", "how", line, f)
        if '"why": ' in line:
            writeHelp("Why is this important? ", "why", line, f)
        if "without_indeni" in line:
            writeHelp("Without Indeni, how could one find this information?", "without_indeni", line, f)
            newLine(f)
        if "ind_url" in line:
            indLink = findToWrite("ind_url", line, 0)
            makeIndHTML(f, indLink, "indURL_" + str(metLinkCount))
            metLinkCount += 1

    makeLinkTableHelp(f, ruleLink, metLinkCount, indNameArr)
    f.close()
    parseLines(restLines, rst_folder)


# Helper method to make the table of links
# Takes: File to write in, String, Int; Returns: Nothing, writes in file
def makeLinkTableHelp(f, ruleLink, metCount, indNameArr):
    helperFillHeadlineRST("To view relevant source code see links below", False, f, "#")

    makeTableBase(f)

    if ".scala" in ruleLink:
        makeRuleTable(f)

    for i in range(metCount):
        makeIndTable(f, indNameArr[i], i)


# Writing helper method to write data into file that is contained in the given key
# Takes: String, String, String, File to write in; Returns: Nothing, writes in file
def writeHelp(title, key, data, f):
    toWrite = findToWrite(key, data, 0)
    makePart(title, toWrite, f)


# Helper method to write message to HomePage
# Takes: String, File to write in; Returns: Nothing, writes to file
def writeToHomePage(toWrite, f):
    f.write(toWrite)
    newLine(f)


# Method that takes a JSON file, with rules and their related metrics and IND scripts,
# And produces RST files organized by Vendors (and possibly Categories, in the future)
# Takes: JSON file; Returns: Nothing, creates several RST files
def generateRst(knowledgeDBFile):
    # Specifies, and possibly creates, folder to place RST files into
    rst_folder = './data/generate_rst'
    if os.path.exists(rst_folder):
        shutil.rmtree(rst_folder)
    os.makedirs(rst_folder)

    jsonFile = open(knowledgeDBFile)
    readFile = jsonFile.readlines()


    # Creates Home RST file, writes in it throughout the duration of the program's runtime
    g = open(createFileName(rst_folder + "/index", "", "", True, False, ""), "a+")
    makeHomePage(g, rst_folder)

    parseLines(readFile, rst_folder)
    g.close()
