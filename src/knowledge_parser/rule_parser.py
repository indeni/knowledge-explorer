from .rule_parsing.extractor import *
from .rule_parsing.tbr_extractor import *
from .rule_parsing.mapper import final_map, to_csv
from .settings import *

def rule_parser(knwoledge_directory, knowledge_branch):

    # Traversing the /rules dir to find all .scala files
    scala_files = find_files(knwoledge_directory, RULES_FILE_ENDING)

    rule_files_lst = select_files(scala_files, IGNORED)

    rules_lst = read_rules(rule_files_lst)

    filtered_rules, commented_out_rules = filter_commented_out_rules(rules_lst)

    rule_names_lst, unproc_rule_names = extract_rule_names(filtered_rules, IGNORED_HELPER_RULES)

    unproc_rule_names_t2 = []
    if unproc_rule_names:
        rule_name_lst_t2, unproc_rule_names_t2 = extract_rule_names_t2(unproc_rule_names)
        rule_names_lst.extend(rule_name_lst_t2)


    unproc_rule_names_tbr= []
    rule_names_tbr = []
    if unproc_rule_names_t2:
        rule_names_tbr, unproc_rule_names_tbr = extract_rule_names_tbr(unproc_rule_names_t2)

    alert_headlines, unproc_headlines = extract_headlines(rule_names_lst)

    alert_severity, unproc_severity = extract_severity(alert_headlines)

    desc_list, unproc_desc = extract_desc(alert_severity)

    metric_lst, unproc_metrics = extract_metrics(desc_list)

    unproc_metrics_t2 = []
    if unproc_metrics:
        metrics_t2, unproc_metrics_t2 = extract_metrics_t2(unproc_metrics)
        metric_lst.extend(metrics_t2)

    unproc_metrics_t3 = []
    if unproc_metrics_t2:
        metrics_t3, unproc_metrics_t3 = extract_metrics_t3(unproc_metrics_t2)
        metric_lst.extend(metrics_t3)

    desc_remed_list, unproc_desc_remed = extract_cond_remed(metric_lst)


    unproc_remed = []
    if unproc_desc_remed:
        remed_list, unproc_remed = extract_remed(unproc_desc_remed)
        desc_remed_list.extend(remed_list)

    rule_categories, unproc_categories = extract_category(desc_remed_list)



    # # Processing dataplane_pool_usage rules.
    unproc_dprc = unproc_rule_names_tbr
    # dpur_lst, unproc_dprc, base_DPUR = process_dpur(unproc_rule_names_tbr, rule_categories)
    #
    # rule_categories.remove(base_DPUR)  # Removing DataplanePoolUsageRule used as template.
    #
    # rule_categories.extend(dpur_lst)

    expanded_metrics = expand_metrics(rule_categories)

    # Template based rules parsing.
    if rule_names_tbr:
        tbr_hl_lst, tbr_unproc_hl = tbr_extract_hl(rule_names_tbr)  # chaining separated tb rules.

        tbr_alert_severity_lst = tbr_extract_severity(tbr_hl_lst)

        tbr_desc_lst, tbr_unproc_desc = tbr_extract_desc(tbr_alert_severity_lst)

        tbr_metrics_lst, tbr_unproc_metrics = tbr_extract_metric(tbr_desc_lst)

        tbr_base_remed_lst, tbr_unproc_base_remed = tbr_extract_base_remed(tbr_metrics_lst)

        tbr_cond_remed_lst = tbr_extract_cond_remed(tbr_base_remed_lst)

        tbr_expanded_metrics = tbr_expand_rules(tbr_cond_remed_lst)

        expanded_metrics.extend(tbr_expanded_metrics)

        tbr_unproc_inv = [tbr_unproc_hl, tbr_unproc_metrics, tbr_unproc_desc, tbr_unproc_base_remed]

        tbr_unproc_lst = status_report_tbr(tbr_unproc_inv)

        if not tbr_unproc_lst:
            print("All data categories from template based rules were extracted successfully.")


    # Formatting for all the rules.
    #format_results = final_format(expanded_metrics)
    format_results = expanded_metrics

    base_ind_url = BASE_URL + '/src/' + knowledge_branch + '/rules'
    final_dicts = final_map(format_results, base_ind_url)

    # Writing the final output to csv.
    rules_output_dir = BASE_OUTPUT_DIR + knowledge_branch + '/rules/'


    outfile = to_csv(rules_output_dir, RULES_FIELDNAMES, final_dicts)
    print('The extracted data saved in the output csv file:', outfile)

    unproc_inv = [unproc_dprc, unproc_headlines, unproc_severity, unproc_metrics_t3,
                  unproc_remed]

    unproc_lst = status_report(unproc_inv)

    if not unproc_lst:
        print("All data categories from single rules were extracted successfully.")

    if commented_out_rules:
        print("Commented out rules found:")
        for r in commented_out_rules:
            print(r)
