import os
import datetime as dt
import pandas as pd
import shutil

# Gets newest csv file from output dir.
def get_newest_csv(output_dir):
    files = [os.path.join(output_dir, x) for x in os.listdir(output_dir) if x.endswith('.csv')]
    newest = max(files, key=os.path.getctime)
    return newest


# Function for the dataframe output to csv.
# Full output filename contains the date of writing in the name.
def df_to_csv(output_dir, df, base_filename):
    output_date = dt.datetime.today().strftime("%Y%m%d")
    outfile = os.path.join(output_dir, output_date + base_filename)
    if os.path.exists(output_dir):
        shutil.rmtree(output_dir)
    os.makedirs(os.path.dirname(output_dir))
    df.to_csv(outfile, index=False)
    return outfile


# Function to filter by values in vendor and os columns from the output table, e.g., 'cisco',
# 'nxos'. The output csv files are saved in 'filtered' directory.
# Full output filename contains the date of writing in the name.
def filter_to_csv(vendor_key, os_key, filtered_output_dir, df):
    filtered_df = df.loc[df['vendor'] == vendor_key]
    if os_key not in ['All', 'all']:
        filtered_df = filtered_df.loc[df['os'] == os_key]
    name_slug = vendor_key + '_' + os_key
    filtered_sorted_df = filtered_df.sort_values('os')
    file_name = '_' + name_slug + '_output.csv'
    outfile = df_to_csv(filtered_output_dir, filtered_sorted_df, file_name)
    return outfile
