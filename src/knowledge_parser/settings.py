# Settings


# 1. Download  and output settings:
BRANCH = 'master'  # Set the git branch name (in quotes) to download , e.g., 'master', 'staging'.
BASE_URL = 'https://bitbucket.org/indeni/indeni-knowledge'
BASE_DOWNLOAD_DIR = './data/downloads/'
BASE_OUTPUT_DIR = './data/output/'

# 2. ind files parsing settings
IND_FILE_ENDING = '.ind.yaml'
REQUIRED_KEYS = {'how', 'why', 'without-indeni', 'can-with-snmp', 'can-with-syslog'}
IND_FIELDNAMES = ['name', 'monitoring_interval', 'vendor', 'os.name', 'chassis', 'product',
                  'metric', 'why', 'how', 'without-indeni', 'can-with-snmp', 'can-with-syslog',
                  'all_fields', 'note', 'ind_url']

# IGNORED_METRICS = ['memory-usage', 'routes-usage', 'hardware-element-status', 'certificate-expiration']
ERROR_PRINTOUT = True  # Set to True for error print out, False to not print out.

# 3. rules files parsing settings.
RULES_FILE_ENDING = '.scala'
RULES_FIELDNAMES = ['rule_file', 'rule_name', 'metric', 'alert_headline', 'alert_severity',
                    'alert_description', 'vendor', 'alert_remediation', 'rule_categories', 'rule_url']
IGNORED = ['Template', 'Interrogation']
IGNORED_HELPER_RULES = ['RuleHelper.scala', 'PerAllowedDeviceRule.scala']

# 4. Google sheets update settings.
GSHEETS_UPDATE = False  # Set to True for updating, False to not update.
GSHEETS_FILE = 'Alert Export'

# 5. Filtering settings
FILTERING = True  # Set to True to produce filtered output csv files.
FILTER_VENDOR = 'checkpoint'  # Set vendor name from the vendor column, e.g. 'checkpoint'.
FILTER_OS = 'all'  # Set os name from the os column, e.g. 'nxos'. Set to 'all' if no filtering by os is required.
