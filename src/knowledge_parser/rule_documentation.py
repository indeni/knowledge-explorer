

# Class RuleDocumentationObject that is held in the RuleDoc Array
# Contains rule data, and all independent RST files will be based on these objects
class RuleDocumentation:

    # RuleName: Name of Rule being used
    # RuleFile: Name of Rule File (should end in .scala)
    # Alert_Headline: Headline of RST page
    # Alert_Description: Description of this Rule
    # Alert_Remediation: How to fix systems if rule is triggered
    # Vendor: Name of Vendor (Cisco, Check Point, etc.)
    # OS: Name of Operating system (nxos, iOS, etc.)
    # metrics: All metrics and their corresponding Script data that is associated with this rule

    def __init__(self, rule_combine_title, rule_combine_alert_headline, rule_name, rule_file, alert_headline,
                 alert_description, alert_remediation, vendor, os, rule_categories, rule_severity, rule_url):
        self.rule_combine_title = rule_combine_title
        self.rule_combine_alert_headline = rule_combine_alert_headline
        self.rule_name = rule_name
        self.rule_file = rule_file
        self.rule_categories = rule_categories
        self.rule_severity = rule_severity
        self.alert_headline = alert_headline
        self.alert_description = alert_description
        self.alert_remediation = alert_remediation
        self.vendor = vendor
        self.os = os
        self.metrics = []  # from type Metric
        self.rule_url = rule_url

    # Helper method to add a metricObj to a ruleDocumentationObj
    # Takes: MetricObj, RuleDocObj; Returns: Nothing, appends together
    def add_data_metric(self, data):
        self.metrics.append(data)


# Class MetricObject that is held in the array of RuleDocumentationObject
# Contains Metric name, and Script data related to this metric
class Metric:

    # metric_name: Name of metric
    # metric_description: All different IND Scripts and Script Descriptions related to this metric
    def __init__(self, metric_name):
        self.metric_name = metric_name
        self.metric_description = []

    # Helper method to add a metricDesc to a metricObj
    # Takes: metricDesc, metricObj; Returns: Nothing, appends together
    def add_data_metric_description(self, data):
        self.metric_description.append(data)


# Class MetricDescription that is held in the array of MetricObject
# Contains important data related to a given metric
class MetricDescription:

    # How: How information about the metric is found
    # Why: Why retrieving this information is important
    # Without_Indeni: How this information would be found by IT admins without Indeni
    # IND_URL: Link to script code
    def __init__(self, script_name, how, why, without_indeni, ind_url):
        self.script_name = script_name
        self.how = how
        self.why = why
        self.without_indeni = without_indeni
        self.ind_url = ind_url
