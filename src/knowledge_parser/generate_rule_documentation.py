import os
import csv
import json
import shutil
import re
from knowledge_generator.helperMethods import newLine, addSpaces, osNameFalse, addOsOrNothing, osHandle
from knowledge_generator.helperMethods import makeRuleLink, handlePlus, findPathFromJSON
from .rule_documentation import RuleDocumentation, Metric, MetricDescription


# Method that converts the Knowledge CSV file into an array, to streamline Rule File parsing
# Takes: CSV File; Returns: Array
def rule_documentation_builder(knowledge_db_file):
    rule_file           = 0
    rule_name           = 1
    metric_index        = 2
    alert_headline      = 3
    alert_severity      = 4
    alert_description   = 5
    alert_remediation   = 6
    rule_url            = 7
    rule_categories_index= 8
    name                = 9
    monitoring_interval = 10
    vendor              = 11
    os                  = 12
    chassis             = 13
    product             = 14
    why                 = 15
    how                 = 16
    without_indeni      = 17
    can_with_snmp       = 18
    can_with_syslog     = 19
    all_fields          = 20
    ind_url             = 21

    # Array of ruleDocumentationObject objects
    rules_documentation = []  # from type RuleDocumentation

    with open(knowledge_db_file) as csv_file:
        knowledge_parser = list(csv.reader(csv_file, delimiter=','))
        first_line = True

        for i in range(len(knowledge_parser)):
            is_same_vendor = False
            is_same_os = False
            is_same_rule_file = False
            if first_line:
                first_line = False
                continue

            data_put = False
            for j in range(len(rules_documentation)):
                if knowledge_parser[i][vendor] == rules_documentation[j].vendor:
                    is_same_vendor = True
                if knowledge_parser[i][os] == rules_documentation[j].os:
                    is_same_os = True
                if knowledge_parser[i][rule_name] == rules_documentation[j].rule_name:
                    is_same_rule_file = True

                if is_same_vendor and is_same_os and is_same_rule_file:
                    row = knowledge_parser[i]
                    metric_description = MetricDescription(row[name], row[how], row[why], row[without_indeni], row[ind_url])

                    for x in range(len(rules_documentation[j].metrics)):
                        if row[metric_index] == rules_documentation[j].metrics[x].metric_name:
                            rules_documentation[j].metrics[x].metric_description.append(metric_description)
                            data_put = True
                            break

                    if not data_put:
                        metric = Metric(row[metric_index])
                        metric.add_data_metric_description(metric_description)
                        rules_documentation[j].metrics.append(metric)
                        data_put = True
                        break

                is_same_rule_file = False
                is_same_os = False
                is_same_vendor = False

            if not data_put:
                row = knowledge_parser[i]
                metric_description = MetricDescription(row[name], row[how], row[why], row[without_indeni], row[ind_url])
                metric = Metric(row[metric_index])
                metric.add_data_metric_description(metric_description)
                rule_combine_title = row[rule_name] + "-" + row[vendor] + "-" + row[os]
                modify_alert_headline = row[alert_headline].replace('" + poolName + "', '')
                rule_combine_alert_headline = modify_alert_headline + "-" + row[vendor] + "-" + row[os]
                rule_categories = row[rule_categories_index].split(',') if row[rule_categories_index] != '' else []
                rule_documentation = RuleDocumentation(
                    rule_combine_title,
                    rule_combine_alert_headline,
                    row[rule_name],
                    row[rule_file],
                    modify_alert_headline,
                    row[alert_description].replace('" + poolName + "', ''),
                    row[alert_remediation],
                    row[vendor],
                    row[os],
                    rule_categories,
                    row[alert_severity].lower(),
                    row[rule_url])
                rule_documentation.add_data_metric(metric)
                rules_documentation.append(rule_documentation)

    return rules_documentation


def save_rule_documentation_as_json(rules_documentation, knowledge_version):
    kb_json = json.dumps(rules_documentation, default=lambda x: x.__dict__, indent=4)
    output_folder = './data/output'
    knowledge_db_json_file = "knowledge.db.{0}.json".format(knowledge_version)
    knowledge_db_json_file_path = output_folder + "/" + knowledge_db_json_file
    db_file = open(knowledge_db_json_file_path, "w")

    db_file.write(kb_json)
    return knowledge_db_json_file_path


# Helper method to create and return a JSON file from a sorted array,
# Takes: Array; Returns: JSON-like File, with parsing hints
def save_rule_documentation_as_rst_for_generation(sorted_arr):
    rst_folder = './data/output'
    knowledge_documentation_json_file = "knowledgeDocumentation.rst.txt"
    knowledge_documentation_json_file_path = rst_folder + "/" + knowledge_documentation_json_file

    f = open(knowledge_documentation_json_file_path, "w")

    f.write("{")
    for obj in range(len(sorted_arr)):
        newLine(f)
        writeRules(sorted_arr[obj], f)
        newLine(f)

    f.write("};")
    f.close()

    return knowledge_documentation_json_file_path


# Helper method that writes key/value pair of Rule data into file
# Takes: String, String, File to write in; Returns: Nothing, writes in file
def ruleHelper(key, val, f):
    if '" +' in val:
        value = handlePlus(val)
    else:
        value = val
    f.write(addSpaces() + addSpaces() + '"' + key + '": "' + value + '"' + ",:")
    newLine(f)


# Helper method that writes blocks of rules as key/value pairs, using helper methods for certain fields
# Takes: ruleDocObject, file to write in; Returns: Nothing, writes in file
def writeRules(rules_documentation, f):
    f.write(
        addSpaces() + '"' + rules_documentation.rule_name + "-" + rules_documentation.vendor + addOsOrNothing(
            rules_documentation.os) + '":: (')
    newLine(f)
    ruleHelper("alertHeadline", rules_documentation.alert_headline + "-" + rules_documentation.vendor + addOsOrNothing(
        rules_documentation.os), f)
    ruleHelper("vendor", rules_documentation.vendor, f)
    if rules_documentation.os.lower() != "false":
        ruleHelper("OS", rules_documentation.os, f)
    ruleHelper("ruleName", rules_documentation.rule_name, f)
    ruleHelper("alertDescription", rules_documentation.alert_description, f)
    ruleHelper("alertRemediation", rules_documentation.alert_remediation, f)
    f.write(addSpaces() + addSpaces() + '"' + "ruleFile" + '": ' + makeRuleLink(rules_documentation.rule_file) + ",;")
    newLine(f)
    f.write(addSpaces() + addSpaces() + '"' + 'metricObjs' + '":; [')
    newLine(f)
    writeMetricObjs(rules_documentation.metrics, f)
    f.write(addSpaces() + ");,")
    newLine(f)


# Helper method that writes key-value pair for metricObjects
# Takes: String, String, File to write in; Returns: Nothing, writes in file
def metObHelp(key, val, f):
    f.write(addSpaces() + addSpaces() + addSpaces() + '"' + key + '": "' + val + '"' + ",:")
    newLine(f)


# Helper method for building metricObject blocks in the JSON file
# Takes: metricObject, File to write in; Returns: Nothing, writes in file
def writeMetricObjs(metrics, f):
    for obj in range(len(metrics)):
        writeMetDes(metrics[obj].metric_name, metrics[obj].metric_description, f)
    f.write(addSpaces() + addSpaces() + "]")
    newLine(f)


# Helper method to write metricDescription fields into file as key-value pair
# Takes: String, String, file to write in; Returns: Nothing, writes in file
def metDesHelp(key, val, f):
    f.write(addSpaces() + addSpaces() + addSpaces() + addSpaces() + '"' + key + '": "' + val + '"' + ",:")
    newLine(f)


# Helper method for building metricDescription blocks in the JSON File
# Takes: String, Array, File to write in; Returns: Nothing, writes in file
def writeMetDes(metName, metDescription, f):
    for metDes in metDescription:
        metObHelp("metricName", metName, f)
        f.write(addSpaces() + addSpaces() + addSpaces() + '"' + 'metricDescription' + '":; [')
        newLine(f)
        metDesHelp("scriptName", metDes.script_name, f)
        metDesHelp("how", metDes.how, f)
        metDesHelp("why", metDes.why, f)
        metDesHelp("without_indeni", metDes.without_indeni, f)
        metDesHelp("ind_url", metDes.ind_url, f)
        f.write(addSpaces() + addSpaces() + addSpaces() + "],")
        newLine(f)
