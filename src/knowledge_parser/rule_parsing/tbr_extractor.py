import os
import re
from .extractor import match_vendor_by_cat


# Extracting template based rule names.
# Return list element structure: (file, rule_text, rule_name).
def tbr_extract_rule_names(r_lst):
    rule_name_lst, unproc_rule_names = [], []
    for r in r_lst:
        rule_name_expr = re.search('ruleName\s=\s\"+.*?\"+', r[1], flags=re.DOTALL)
        if rule_name_expr:
            rule_name = rule_name_expr.group(0).replace('ruleName = ', '').strip('"')
            rule_name_lst.append((r[0], r[1], rule_name))
        else:
            unproc_rule_names.append(r)
    return rule_name_lst, unproc_rule_names


# Extracting alert headlines.
# Return list element structure: (file, rule_text, rule_name, alert_full).
def tbr_extract_hl(r_lst):
    hl_lst, unproc_hl = [], []
    for r in r_lst:
        al_headline_expr = re.search('ruleFriendlyName\s=\s\"+.*?\"+', r[1], flags=re.DOTALL)
        if al_headline_expr:
            al_headline_full = al_headline_expr.group(0).replace('ruleFriendlyName = ', '').strip('"')
            al_headline_split = al_headline_full.split(':')
            alert_cat = al_headline_split[0].strip()
            if len(al_headline_split) > 1:
                al_headline = al_headline_split[1].strip()
            else:
                al_headline = ''
            alert_full = (alert_cat, al_headline)
            hl_lst.append((r[0], r[1], r[2], alert_full))
        else:
            unproc_hl.append(r)
    return hl_lst, unproc_hl


# Extracting alert severity.
# Return list element structure: (file, rule_text, rule_name, alert_full, alert_severity).
def tbr_extract_severity(rules_lst):
    tbr_alert_severity_lst = []
    for r in rules_lst:
        match = re.search('AlertSeverity.\w*?[,)]+', r[1], flags=re.DOTALL)
        if match:
            severity_exp = match.group(0)
            severity = severity_exp.split('.')[1].strip(',) ')
        else:
            severity = 'ERROR'
        tbr_alert_severity_lst.append((r[0], r[1], r[2], r[3], severity))
    return tbr_alert_severity_lst


# Extracting descriptions.
# Return list element structure: (file, rule_text, rule_name, alert_full, alert_severity,
#  alert_desc).
def tbr_extract_desc(r_lst):
    desc_lst, unproc_desc = [], []
    for r in r_lst:
        al_desc_expr = re.search('ruleDescription\s=.*?"+.*?(?<!\\\)"+', r[1], flags=re.DOTALL)
        if al_desc_expr:
            al_desc = al_desc_expr.group(0)
            if '"""' in al_desc:
                triple_q_results = re.findall('ruleDescription\s=+.*?"{3}.*?"{3}\.+', r[1],
                                              flags=re.DOTALL)
                if triple_q_results:
                    raw_desc = triple_q_results[0].strip().strip('.')
            else:
                raw_desc = al_desc
            desc = raw_desc.split('=')[1].strip().strip('"')
            desc_lst.append((r[0], r[1], r[2], r[3], r[4], desc))
        else:
            unproc_desc.append(r)
    return desc_lst, unproc_desc


# Extracting metrics.
# Return list element structure: (file, rule_text, rule_name, alert_full, alert_severity,
#  alert_desc, metric_list).
def tbr_extract_metric(r_lst):
    metrics_lst, unproc_metrics = [], []
    for r in r_lst:
        metric_expr = re.findall('[m|M]etricName\s=\s\"+.*?\"+', r[1])
        if metric_expr:
            metric_lst = [x.split('=')[1].strip().strip('"') for x in metric_expr]
            metrics_lst.append((r[0], r[1], r[2], r[3], r[4], r[5], metric_lst))
        else:
            unproc_metrics.append(r)
    return metrics_lst, unproc_metrics


# Extracting base remediation.
# Return list element structure: (file, rule_text, rule_name, alert_full, alert_severity,
#  alert_desc, metric_list, base_remed).
def tbr_extract_base_remed(r_lst):
    base_remed_lst, unproc_base_remed = [], []
    for r in r_lst:
        base_remed_expr, raw_base_remed = '', ''
        base_remed_expr1 = re.search('(baseRemediationText|alertRemediationSteps)\s=\s\"+.*?\.\"+',
                                     r[1], flags=re.DOTALL)
        if base_remed_expr1:
            base_remed_expr = base_remed_expr1
        else:
            base_remed_expr2 = re.search('(baseRemediationText|alertRemediationSteps)\s=\s.*?\"+.*?(?<!\\\)\"+',
                                         r[1], flags=re.DOTALL)
            if base_remed_expr2:
                base_remed_expr = base_remed_expr2
        if base_remed_expr:
            base_remed = base_remed_expr.group(0)
            if '"""' in base_remed:
                triple_q_results = re.findall('baseRemediationText\s=+.*?"{3}.*?"{3}', r[1],
                                              flags=re.DOTALL)
                if triple_q_results:
                    raw_base_remed = triple_q_results[0].strip().strip('.')
            else:
                raw_base_remed = base_remed
            clean_base_remed = raw_base_remed.replace('baseRemediationText =', '').replace('alertRemediationSteps =',
                                                                                           '').strip().strip('"')
            base_remed_lst.append((r[0], r[1], r[2], r[3], r[4], r[5], r[6], clean_base_remed))
        else:
            unproc_base_remed.append(r)
    return base_remed_lst, unproc_base_remed


# Function finds ConditionalRemediation steps in a template based rule.
# Returns a list.
def find_cond_remed_rule_tbr(rule):
    remed_list = []
    results = re.findall('ConditionalRemediationSteps\.+.*?\".*?(?<!\\\)\"+', rule, flags=re.DOTALL)
    if results:
        joined_results = []
        triple_g_filtering = False
        for res in results:
            if '"""' in res:
                if not triple_g_filtering:
                    triple_g_filtering = True
                    triple_q_results = re.findall('ConditionalRemediationSteps\.\w*?_\w*?\s->\s*?"{3}.*?"{3}\.', rule,
                                                  flags=re.DOTALL)
                    joined_results.extend(triple_q_results)
            else:
                joined_results.append(res)
        remed_list = [x.replace('ConditionalRemediationSteps', '').strip('().').strip().strip('"') for x in
                      joined_results]
    return remed_list



# Extracting conditional remediation.
# Return list element structure: (file, rule_text, rule_name, alert_full, alert_severity,
#  alert_desc, metric_list, base_remed, cond_remed).
def tbr_extract_cond_remed(r_lst):
    cond_remed_lst = []
    for r in r_lst:
        rule_cond_remed = []
        remed_list = find_cond_remed_rule_tbr(r[1])
        if remed_list:
            vendor_by_cat = match_vendor_by_cat(r[3])
            if vendor_by_cat == 'All':
                rule_cond_remed.append((vendor_by_cat, ''))
            for v in remed_list:
                vendor = v.split('->')[0].replace('VENDOR_', '').replace('OS_', '').strip()
                vendor_remed_raw = v.split('->')[1:]
                vendor_remed = '->'.join(vendor_remed_raw).strip().strip('"')
                rule_cond_remed.append((vendor, vendor_remed))
            cond_remed_lst.append((r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], rule_cond_remed))
        else:
            vendor_by_cat = match_vendor_by_cat(r[3])
            rule_cond_remed.append((vendor_by_cat, ''))
            cond_remed_lst.append((r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], rule_cond_remed))
    return cond_remed_lst


# Separating the rules by single metrics and by vendor.
# Input list element structure: (file, rule_text, rule_name, alert_full, alert_severity,
# alert_desc, metric_list, base_remed, cond_remed).
# Return list element structure: (file, rule_text, rule_name, alert_full, alert_severity,
# metric, desc_remed).

def tbr_expand_rules(r_lst):
    expanded_rule_data = []
    for r in r_lst:
        for metric in r[6]:
            for v in r[8]:
                if v[1] == '':
                    cond_remed_full = r[7]
                else:
                    cond_remed_full = r[7] + '\n' + v[1]
                expanded_rule_data.append((r[0], r[1], r[2], r[3], r[4],
                                           metric, (r[5], v[0], cond_remed_full)))
    return expanded_rule_data


# Checking the status of unprocessed template based rules lists and reporting it.
def status_report_tbr(unproc_inv):
    unproc_lst = []
    keys = ['alert_headline', 'metric', 'alert_description', 'alert_remediation']
    unproc_dict = dict(zip(keys, unproc_inv))
    for key in unproc_dict:
        if unproc_dict[key]:
            print('Some cases of:', key, 'were NOT EXTRACTED from the following template based rules:')
            for r in unproc_dict[key]:
                rule = r[0]
                print(rule)
                unproc_lst.append((key, rule))
    return unproc_lst
