import cgi
import io
import os
import re
import requests
import zipfile
import shutil
import sys


# Function checks if the file in the repo is newer than in the data dir.
#  If it is newer, downloads and unzips the knowledge dir.
def checked_download_zip(download_dir, url):
    if os.path.exists(download_dir):
        shutil.rmtree(download_dir)
    os.makedirs(os.path.dirname(download_dir))
    head = requests.head(url)
    cont_disp = head.headers['content-disposition']
    value, params = cgi.parse_header(cont_disp)
    dest_filename = params['filename']
    dest_dir = dest_filename.split('.')[0]
    if dest_dir in os.listdir(download_dir):
        last_data_dir = download_dir + dest_dir
        print("The latest knowledge directory is already downloaded.")
    else:
        r = requests.get(url)
        z = zipfile.ZipFile(io.BytesIO(r.content))
        z.extractall(path=download_dir)
        last_data_dir = download_dir + z.namelist()[0]  # get the name of the latest downloaded dir
        print("Downloaded newer version of knowledge directory from the repo.")
    return last_data_dir


# Traversing the last data dir to find all files with set ending.
# Return list element structure: (file).
def find_files(last_data_dir, ending):
    files_list = []
    rules_dir = os.path.join(last_data_dir, 'rules')
    for root, dirs, files in os.walk(rules_dir):
        for f in files:
            if f.endswith(ending):
                files_list.append(os.path.join(root, f))
    return files_list


# Traversing the set files list to find all rule files with and ignore template,
# interrogation and other rule files files set in IGNORED list.
# Return list element structure: (file).
def select_files(file_lst, marker_lst):
    rule_files_lst = []
    for fn in file_lst:
        if all(m not in os.path.basename(fn) for m in marker_lst):
            rule_files_lst.append(fn)
    return rule_files_lst


# Reading rules files.
# Return list element structure: (file, rule_text).
def read_rules(files_list):
    rules_list = []
    for file in files_list:
        with open(file, "r") as f:
            rule = f.read()
            rules_list.append((file, rule))
    return rules_list


# Filtering commented out rules:
# Return list element structure: (file, rule_text)
def filter_commented_out_rules(rules_list):
    filtered_rules, commented_out_rules = [], []
    for r in rules_list:
        lines = r[1].splitlines()
        counter = 0
        for l in lines[:10]:
            if l.startswith('//'):
                counter += 1
        if counter < 10:
            filtered_rules.append(r)
        else:
            commented_out_rules.append(r[0])
    return filtered_rules, commented_out_rules


# Extracting rule names.
# Return list element structure: (file, rule_text, rule_name).
def extract_rule_names(rules_list, ignored_list):
    rule_names_lst, unproc_rule_names = [], []
    for r in rules_list:
        match = re.search('RuleMetadata[\r\n]*\s*\.builder\([\r\n]*\s*\s*\".*?\"+', r[1], flags=re.DOTALL)
        if match:
            meta_expr = match.group(0)
            meta_name = meta_expr.replace('RuleMetadata.builder(', '').strip().strip('"')
            rule_names_lst.append((r[0], r[1], meta_name))
        else:
            rule_filename = os.path.basename(r[0])
            if rule_filename not in ignored_list:
                unproc_rule_names.append(r)
    return rule_names_lst, unproc_rule_names


# Extracting rule names from type 2 rules.
# Return list element structure: (file, rule_text, rule_name).
def extract_rule_names_t2(rules_list):
    rule_name_lst_t2, unproc_rule_names_t2 = [], []
    for r in rules_list:
        rule_name_expr = re.search('NAME\s=\s\"+.*?\"+', r[1], flags=re.DOTALL)
        if rule_name_expr:
            rule_name = rule_name_expr.group(0).replace('NAME = ', '').strip('"')
            rule_name_lst_t2.append((r[0], r[1], rule_name))
        else:
            unproc_rule_names_t2.append(r)
    return rule_name_lst_t2, unproc_rule_names_t2


# Extracting template based rule names.
# Return list element structure: (file, rule_text, rule_name).
def extract_rule_names_tbr(rules_list):
    rule_name_lst, unproc_rule_names = [], []
    for r in rules_list:
        rule_name_expr = re.search('ruleName\s=\s\"+.*?\"+', r[1], flags=re.DOTALL)
        if rule_name_expr:
            rule_name = rule_name_expr.group(0).replace('ruleName = ', '').strip('"')
            rule_name_lst.append((r[0], r[1], rule_name))
        else:
            unproc_rule_names.append(r)
    return rule_name_lst, unproc_rule_names


# Extracting alert headlines.
# Return list element structure: (file, rule_text, rule_name, alert_full).
def extract_headlines(rules_list):
    alert_headlines, unproc_headlines = [], []
    for r in rules_list:
        match = re.search('RuleMetadata[\r\n]*\s*\.builder\([\r\n]*\s*.*?,.*?(?<!\)),+', r[1], flags=re.DOTALL)
        if match:
            meta_data = match.group(0)
            fields = meta_data.split(',')
            alert_headline_full = fields[-2].strip().strip('"')
            alert_headline_split = alert_headline_full.split(':')
            if len(alert_headline_split) < 2:
                alert_headline = alert_headline_full
                alert_cat = 'NA'
            else:
                alert_headline = alert_headline_split[1].strip()
                alert_cat = alert_headline_split[0].strip()
            alert_full = (alert_cat, alert_headline)
            alert_headlines.append((r[0], r[1], r[2], alert_full))
        else:
            unproc_headlines.append(r)
    return alert_headlines, unproc_headlines


# Extracting alert severity.
# Return list element structure: (file, rule_text, rule_name, alert_full, alert_severity).
def extract_severity(rules_lst):
    alert_severity_lst, unproc_severity = [], []
    for r in rules_lst:
        match = re.search('AlertSeverity.(\w+)[,)\n]+', r[1], flags=re.DOTALL)
        if match:
            try:
                severity_exp = match.group(0)
                severity = severity_exp.split('.')[1].strip('\n').strip(',) ')
                alert_severity_lst.append((r[0], r[1], r[2], r[3], severity))
            except:
                print('Error extracting severity: ' + r[0] + ' ' + sys.exc_info()[0])
        else:
            unproc_severity.append(r)
    return alert_severity_lst, unproc_severity


# Function extracts alert description from rules.
# Return list element structure: (file, rule_text, rule_name, alert_full, alert_severity, desc).
def extract_desc(rules_list):
    desc_list, unproc_desc = [], []
    for r in rules_list:
        match = re.search('RuleMetadata[\r\n]*\s*\.builder[\r\n]*\s*\(+.*?,.*?(?<!\)),.*?".*?",+', r[1], flags=re.DOTALL)
        if match:
            meta_data = match.group(0)
            fields = meta_data.split(',')
            desc_trailing = ','.join(fields[2:]).strip()
            ind = desc_trailing.find('"')
            desc = desc_trailing[ind:].strip('",').strip().strip('"')
            desc_list.append((r[0], r[1], r[2], r[3], r[4], desc))
        else:
            unproc_desc.append(r)
    return desc_list, unproc_desc


# Extracting metrics.
# Return list element structure: (file, rule_text, rule_name, alert_full, alert_severity,
# expr_lst, metric).
def extract_metrics(rules_list):
    metric_list = []
    unproc_metrics = []
    for r in rules_list:
        metrics_expr = re.findall('Select\w*TimeSeriesExpression\[Double\]\(+.*?\"+.*?\"+\)+', r[1])
        if metrics_expr:
            for e in metrics_expr:
                metric = e.strip('SelectTimeSeriesExpression[Double](.tsDao, Set( Last').strip(')')
                metric_list.append((r[0], r[1], r[2], r[3], r[4], r[5], metric))
        else:
            unproc_metrics.append(r)
    return metric_list, unproc_metrics


# Extracting unprocessed metrics type 2.
# Return list element structure: (file, rule_text, rule_name, alert_full, alert_severity,
#  expr_lst, metric).
def extract_metrics_t2(unproc_list):
    metrics_t2 = []
    unproc_metrics_t2 = []
    for r in unproc_list:
        metrics_expr = re.findall('SelectSnapshotsExpression\(+.*?\"+.*?\"+\)+', r[1])
        if metrics_expr:
            for e in metrics_expr:
                metric = e.strip('SelectSnapshotExpression(context.snapshotsDao, Set(').strip(')')
                metrics_t2.append((r[0], r[1], r[2], r[3], r[4], r[5], metric))
        else:
            unproc_metrics_t2.append(r)
    return metrics_t2, unproc_metrics_t2


# Extracting unprocessed metrics type 3.
# Return list element structure: (file, rule_text, rule_name, alert_full, alert_severity,
# expr_lst, metric).
def extract_metrics_t3(unproc_list):
    metrics_t3 = []
    post_unproc_metrics = []
    for r in unproc_list:
        metrics_expr = re.findall('val\s+snapshotKey\s+=\s\"+.*?\"+', r[1])
        if metrics_expr:
            for e in metrics_expr:
                metric = e.strip('val snapshotKey = ').strip('"')
                metrics_t3.append((r[0], r[1], r[2], r[3], r[4], r[5], metric))
        else:
            post_unproc_metrics.append(r)
    return metrics_t3, post_unproc_metrics


# Matches vendors by the alert headline category.
def match_vendor_by_cat(alert_full):
    cat = alert_full[0]
    if 'Check Point' in cat:
        vendor = 'CP'
    elif 'Palo Alto Networks' in cat:
        vendor = 'PANOS'
    elif 'Radware' in cat:
        vendor = 'RADWARE'
    elif 'F5' in cat:
        vendor = 'F5'
    elif 'CISCO' in cat:
        vendor = 'CISCO'
    elif 'Linux' in cat:
        vendor = 'LINUX'
    elif 'Fortinet' in cat:
        vendor = 'FORTINET'
    elif 'Juniper' in cat:
        vendor = 'JUNIPER'
    else:
        vendor = 'All'
    return vendor


# Function finds ConditionalRemediation steps in a rule.
# Returns a list.
def find_cond_remed_rule(rule):
    remed_list = []
    results = re.findall('ConditionalRemediationSteps[(.]+.*?\".*?(?<!\\\)\"+', rule, flags=re.DOTALL)
    if results:
        joined_results = []
        triple_g_filtering = False
        for res in results:
            if '"""' in res:
                if not triple_g_filtering:
                    triple_g_filtering = True
                    triple_q_results = re.findall('ConditionalRemediationSteps\.\w*?_\w*?\s->\s*?"{3}.*?"{3}\.', rule,
                                                  flags=re.DOTALL)
                    joined_results.extend(triple_q_results)
            else:
                joined_results.append(res)
        remed_list = [x.replace('ConditionalRemediationSteps', '').strip('().').strip().strip('"') for x in
                      joined_results]
    return remed_list


# Extracting conditional remediation by vendor or os.
# Return list element structure: (file, rule_text, rule_name, alert_full, alert_severity,
#  metric, desc_remed).
def extract_cond_remed(rules_list):
    desc_remed_list, unproc_desc_remed = [], []
    for r in rules_list:
        remed_list = find_cond_remed_rule(r[1])
        if remed_list:
            desc = r[5]
            base_remed = 'NOT EXTRACTED'
            vendor_lst = []
            for rem in remed_list:
                if not any(x in rem for x in ['VENDOR', 'OS']):
                    base_remed = rem
                else:
                    vendor_lst.append(rem)
            if vendor_lst:
                vendor_by_cat = match_vendor_by_cat(r[3])
                if vendor_by_cat == 'All':
                    desc_remed = (desc, vendor_by_cat, base_remed)
                    desc_remed_list.append((r[0], r[1], r[2], r[3], r[4], r[6], desc_remed))
                for v in vendor_lst:
                    vendor = v.split('->')[0].replace('VENDOR_', '').replace('OS_', '').strip()
                    vendor_remed_raw = v.split('->')[1:]
                    vendor_remed = '->'.join(vendor_remed_raw).strip().strip('"')
                    remed = base_remed + '\n|' + '|' + vendor_remed
                    desc_remed = (desc, vendor, remed)
                    desc_remed_list.append((r[0], r[1], r[2], r[3], r[4], r[6], desc_remed))
            else:
                vendor = match_vendor_by_cat(r[3])
                desc_remed = (desc, vendor, base_remed)
                desc_remed_list.append((r[0], r[1], r[2], r[3], r[4], r[6], desc_remed))
        else:
            unproc_desc_remed.append(r)
    return desc_remed_list, unproc_desc_remed


# Extracting base remediation from rules.
# Return list element structure: (file, rule_text, rule_name, alert_full, alert_severity,
#  metric, desc_remed).
def extract_remed(rule_lst):
    remed_list, unproc_remed = [], []
    for r in rule_lst:
        ind = r[1].rfind('ConstantExpression')
        sub = r[1][ind:]
        expr = re.findall('ConstantExpression\(+\s*?".*?(?<!\\\)"+\)+', sub, flags=re.DOTALL)
        if expr:
            remed = expr[0].replace('ConstantExpression(', '').strip(')').strip('"')
            desc = r[5]
            vendor = match_vendor_by_cat(r[3])
            desc_remed = (desc, vendor, remed)
            remed_list.append((r[0], r[1], r[2], r[3], r[4], r[6], desc_remed))
        else:
            unproc_remed.append(r)
    return remed_list, unproc_remed


# Processing dataplane_pool_usage rules. Using the data extracted from DataplanePoolUsageRule
# as base.
# Return list element structure: (file, rule_text, rule_name, alert_full, alert_severity,
#  metric, desc_remed).
def process_dpur(unproc_rule_lst, proc_rule_lst):
    dpur_lst, unproc_dprc = [], []
    base_DPUR = next(r for r in proc_rule_lst if os.path.basename(r[0]) == 'DataplanePoolUsageRule.scala')
    for r in unproc_rule_lst:
        pool_name_expr = re.search('DataplanePoolUsageRule\(context,\s\"+.*?\"+', r[1], flags=re.DOTALL)
        if pool_name_expr:
            pool_name_raw = pool_name_expr.group(0).replace('DataplanePoolUsageRule(context, ', '').strip('"')
            pool_name = pool_name_raw.lower().replace(' ', '_')
            rule_name = 'panw_dataplane_' + pool_name
            alert_cat = 'Palo Alto Networks Firewalls'
            alert_headline = pool_name_raw + ' dataplane pool utilization high'
            alert_full = (alert_cat, alert_headline)
            desc1 = re.sub('poolName', pool_name_raw, base_DPUR[6][0])
            desc2 = re.sub('\+ "', '', desc1)
            desc3 = re.sub('",', '.', desc2)
            desc_remed = (desc3, base_DPUR[6][1], base_DPUR[6][2])
            dpur_lst.append((r[0], base_DPUR[1], rule_name, alert_full, base_DPUR[4],
                             base_DPUR[5], desc_remed))
        else:
            unproc_dprc.append(r)
    return dpur_lst, unproc_dprc, base_DPUR


# Breaking the entries by single metrics.
# Return list element structure: (file, rule_text, rule_name, alert_full, alert_severity, metric, desc_remed).
def expand_metrics(rule_list):
    expanded_metrics = []
    for r in rule_list:
        metrics_split = (r[5]).split(',')
        for m in metrics_split:
            metric = m.strip().strip('"')
            expanded_metrics.append((r[0], r[1], r[2], r[3], r[4], metric, r[6], r[7]))
    return expanded_metrics

# Checking the status of unprocessed rules lists and reporting it.
def status_report(unproc_inv):
    unproc_lst = []
    keys = ['rule_name', 'alert_headline', 'alert_severity', 'metric', 'alert_remediation']
    unproc_dict = dict(zip(keys, unproc_inv))
    for key in unproc_dict:
        if unproc_dict[key]:
            print('Some cases of:', key, 'were NOT EXTRACTED from the following rules:')
            for r in unproc_dict[key]:
                rule = r[0]
                print(rule)
                unproc_lst.append((key, rule))
    return unproc_lst

# Extracting rule categories
# Return list element structure: (file, rule_text, rule_name, alert_full, alert_severity, metric, desc_remed, rule_categories).
def extract_category(rules_lst):
    rule_categories, unproc_categories = [], []
    for r in rules_lst:
        match = re.search('RuleCategory.\w*?[,)]+', r[1], flags=re.DOTALL)
        if match:
            categories_exp = match.group(0)
            categories = categories_exp.split('.')[1].strip(',) ')
            rule_categories.append((r[0], r[1], r[2], r[3], r[4], r[5], r[6], categories))
        else:
            rule_categories.append((r[0], r[1], r[2], r[3], r[4], r[5], r[6], []))
    return rule_categories, unproc_categories
