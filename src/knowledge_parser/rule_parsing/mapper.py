import os
import datetime as dt
import csv
import shutil


# Mapping the data for csv writer.
# Input list element structure: (file, rule_text, rule_name, alert_full, alert_severity,
# metric, desc_remed).
# Return dict element structure: (rule_file, rule_name, metric, alert_headline, alert_severity,
# alert_description, vendor, alert_remediation).
def final_map(rule_list, base_ind_url):
    final_dicts = []
    for r in rule_list:
        final_dict = {}
        final_dict['rule_file'] = os.path.basename(r[0])
        final_dict['rule_name'] = r[2]
        final_dict['alert_headline'] = r[3][1]
        final_dict['alert_severity'] = r[4]
        final_dict['metric'] = r[5]
        keys = ('alert_description', 'vendor', 'alert_remediation')
        values = r[6]
        if len(r) >= 8:
            final_dict['rule_categories'] = r[7]
        final_dict['rule_url'] = gen_rule_url(r[0], base_ind_url)
        rem_dict = dict(zip(keys, values))
        final_dict.update(rem_dict)
        final_dicts.append(final_dict)
    return final_dicts

# Function generates url link to ind file in Bitbucket repo  and appends it to tuple.
def gen_rule_url(rule_path, base_url):
    path = rule_path.split('/rules')
    rule_url = base_url + path[1]
    return rule_url

# Function for the output_old to to csv.
# Output filename contains the date of writing in the name.
def to_csv(output_dir, fieldnames, dicts):
    output_date = dt.datetime.today().strftime("%Y%m%d")
    outfile = os.path.join(output_dir, output_date + '_rules_output.csv')
    if os.path.exists(output_dir):
        shutil.rmtree(output_dir)
    os.makedirs(os.path.dirname(output_dir))
    with open(outfile, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for d in dicts:
            writer.writerow(d)
    return outfile
