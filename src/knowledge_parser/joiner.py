import pandas as pd
import os
import csv

from .joining.utils import get_newest_csv, df_to_csv, filter_to_csv
from .settings import *


def joiner(knowledge_branch):

    # The script gets the latest ind and rules csv files and produces joined  csv table
    # performing left outer join on rules with ind data. The column used for join is 'metric'.

    rules_output_dir = BASE_OUTPUT_DIR + knowledge_branch + '/rules/'
    ind_output_dir = BASE_OUTPUT_DIR + knowledge_branch + '/ind/'

    rules_csv = get_newest_csv(rules_output_dir)
    ind_csv = get_newest_csv(ind_output_dir)

    rule_categories_csv = './knowledge_parser/rule_categories_mapping.csv'

    rules_df = pd.read_csv(rules_csv)
    ind_df = pd.read_csv(ind_csv)
    rule_categories_df = pd.read_csv(rule_categories_csv)

    # Renaming rules vendors to the same format as ind vendors.
    vendors_dict = {'CP': 'checkpoint', 'CISCO': 'cisco', 'F5': 'f5', 'PANOS': 'paloaltonetworks',
                    'JUNIPER': 'juniper', 'All': 'all', 'OTHER': 'other', 'NXOS': 'nxos',
                    'RADWARE': 'radware', 'LINUX': 'linux', 'FORTINET': 'fortinet', 'RHEL': 'rhel',
                    'ZSCALER': 'zscaler', 'BLUECAT': 'bluecat' }

    rules_df['vendor'].replace(vendors_dict, inplace=True)

    joined_rule_with_categories_df = \
        pd\
            .merge(rules_df, rule_categories_df, how='left', on='rule_name')\
            .drop(['rule_categories_x'], axis=1)
    joined_rule_with_categories_df.rename(columns={'rule_categories_y': 'rule_categories'}, inplace=True)

    # Joining the rules dataframe with the ind dataframe. Performing left outer join.
    joined_df = pd.merge(joined_rule_with_categories_df, ind_df, how='left', on='metric')

    # Changing os.name column to format suitable for query.
    joined_df.rename(columns={'os.name': 'os'}, inplace=True)
    joined_df['os'].replace({'False': ''}, inplace=True)

    # Matching vendors in rules and ind files. Vendors indcated as 'all' or 'other' are included as well.
    vendor_matched_df = joined_df.query('vendor_x == vendor_y or vendor_x in ["all", "other"]'
                                        ' or vendor_x == os')
    vendor_matched_unique_df = vendor_matched_df.drop_duplicates().copy()

    # vendor_matched_unique_df.fillna('Not available', inplace=True)


    # Transfering rules vendor NXOS into ind vendor for Cisco.
    vendor_matched_unique_df.loc[vendor_matched_unique_df.vendor_x == 'nxos', 'vendor_y'] = 'cisco NXOS'


    vendor_x_dropped_df = vendor_matched_unique_df.drop(['vendor_x'], axis=1)

    # Dropping duplicates rows.
    duplicated_remed_df = vendor_x_dropped_df.drop_duplicates().copy()
    duplicated_remed_df.rename(columns={'vendor_y': 'vendor'}, inplace=True)

    columns = list(duplicated_remed_df)
    columns.remove('alert_remediation')

    duplicated_os_df = duplicated_remed_df.sort_values('alert_remediation', ascending=False).drop_duplicates(subset=columns).sort_index().copy()


    metric_df = duplicated_os_df.sort_values('vendor', ascending=False).drop_duplicates(subset=['rule_file', 'rule_name', 'metric', 'name']).sort_index().copy()
    metric_df['vendor'].replace({'cisco NXOS': 'cisco'}, inplace=True)
    metric_df['vendor'].replace({'False': ''}, inplace=True)


    # Creating dataframe for uploading alerts to a webpage.
    web_df = metric_df.drop(['metric'], axis=1)
    web_df.drop_duplicates(inplace=True)


    # Reindexing the dataframe to better fit Google sheets updating.
    web_df = web_df.reset_index(drop=True)
    web_df.index = web_df.index.map(str)


    # Removing lines with missing fields
    web_df = web_df[web_df.why != 'Error: missing field']
    web_df = web_df[web_df.why != 'Not available']

    web_df = web_df[web_df.how != 'Error: missing field']
    web_df = web_df[web_df.how != 'Not available']

    web_df = web_df[web_df.vendor != 'False']

    # ----------------------------------------------------------------
    # -------------------- START CODE NOT IN USE ---------------------
    # ----------------------------------------------------------------

    # Last edit for final_df.
    final_df = web_df

    # Renaming the columns to mach the ACF field names.
    web_df.rename(columns={'alert_description': 'description', 'alert_remediation': 'steps',
                                 'monitoring_interval': 'interval', 'without-indeni': 'without',
                                 'ind_url': 'link'}, inplace=True)

    vendors_caps_dict = {'checkpoint': 'Check Point', 'cisco': 'Cisco', 'paloaltonetworks': 'Palo Alto Networks',
                         'f5': 'F5', 'juniper': 'Juniper', 'radware': 'Radware', 'linux': 'Linux', 'fortinet': 'Fortinet',
                         'rhel': 'RHEL', 'zscaler': 'Zscaler', 'bluecat': 'BlueCat'}

    web_df['vendor'].replace(vendors_caps_dict, inplace=True)

    web_df['alerts_category'] = web_df['vendor']
    web_df.loc[web_df.os == 'nxos', 'alerts_category'] = 'Cisco Nexus'

    web_df['post_title'] = web_df['alert_headline'].astype(str) + ' for ' + web_df['alerts_category']
    web_df.drop_duplicates('post_title', inplace=True)
    web_df.sort_values('vendor', inplace=True)


    # Filtering for officially supported devices only.
    official_list = ['Check Point', 'Palo Alto Networks', 'F5', 'Juniper', 'Fortinet', 'Radware'] #remove Cisco
    web_df = web_df.loc[web_df['vendor'].isin(official_list)]

    web_df = web_df.loc[web_df['os'] != 'ios']

    # Writing to csv files.
    joined_output_dir = BASE_OUTPUT_DIR + BRANCH + '/joined/'
    web_output_dir = BASE_OUTPUT_DIR + BRANCH + '/web/'

    # ----------------------------------------------------------------
    # ----------------------END CODE NOT IN USE ----------------------
    # ----------------------------------------------------------------

    #metric_df['vendor'].replace(vendors_caps_dict, inplace=True)
    official_list_lower_case = ['checkpoint', 'paloaltonetworks', 'f5', 'juniper', 'radware',
                                'fortinet', 'bluecoat', 'rhel', 'zscaler', 'bluecat']  # remove Cisco

    metric_df = metric_df.loc[metric_df['vendor'].isin(official_list_lower_case)]

    metric_outfile = df_to_csv(joined_output_dir, metric_df, '_metric_output.csv')
    print('The output with metrics saved in the csv file:', metric_outfile)

    # Sorting metric dataframe.
    sorted_df = metric_df.sort_values(['vendor', 'os'])

    #Remove all rows where the vendor is false
    sorted_df = sorted_df[sorted_df['vendor'] != "False"]
    sorted_df = sorted_df[sorted_df['vendor'] != None]

    sorted_df['how'].replace({'Error: missing field' : ''}, inplace=True)
    sorted_df['why'].replace({'Error: missing field': ''}, inplace=True)
    sorted_df['without-indeni'].replace({'Error: missing field': ''}, inplace=True)

    # Remove every row in the sorted file where there is a "Not available" or missing field message
    # for col in sorted_df:
    #     if col != "chassis" and col != "product":
    #         sorted_df = sorted_df[sorted_df[col] != "Not available"]
    #     sorted_df = sorted_df[sorted_df[col] != "Error: missing field"]

    sorted_outfile = df_to_csv(joined_output_dir, sorted_df, '_sorted_output.csv')

    print('The sorted output saved in the csv file:', sorted_outfile)

    return sorted_outfile


if __name__ == '__main__':

    knowledgeDBFile = joiner('hotfix/IK-2830_Add_categorize_to_all_rules')

