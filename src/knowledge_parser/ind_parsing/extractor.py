import cgi
import os
import io
import re
import zipfile
import requests
import yaml
import shutil

# Function checks if the file in the repo is newer than in the data dir.
#  If it is newer, downloads and unzips the knowledge dir.
def checked_download_zip(download_dir, url):
    if os.path.exists(download_dir):
        shutil.rmtree(download_dir)
    os.makedirs(os.path.dirname(download_dir))
    head = requests.head(url)
    cont_disp = head.headers['content-disposition']
    value, params = cgi.parse_header(cont_disp)
    dest_filename = params['filename']
    dest_dir = dest_filename.split('.')[0]
    if dest_dir in os.listdir(download_dir):
        last_data_dir = download_dir + dest_dir
        print("The latest knowledge directory is already downloaded.")
    else:
        r = requests.get(url)
        z = zipfile.ZipFile(io.BytesIO(r.content))
        z.extractall(path=download_dir)
        last_data_dir = download_dir + z.namelist()[0]  # get the name of the latest downloaded dir
        print("Downloaded newer version of knowledge directory from the repo.")
    return last_data_dir


# Traversing the last data dir to find all files with set ending.
def find_files(last_data_dir, ending):
    files_list = []
    for root, dirs, files in os.walk(last_data_dir):
        for f in files:
            if f.endswith(ending):
                files_list.append(os.path.join(root, f))
    return files_list


# Getting all the files with the target section.
def find_target(files_list, target):
    target_files = []
    for file in files_list:
        with open(file, "r") as f:
            if target in f.read():
                target_files.append(file)
    return target_files


# Functions used in extracting the data from the knowledge dir.

def get_raw_data(files_list):
    raw_data_list = []
    for file in files_list:
        with open(file, "r") as f:
            doc = f.read()
            raw_data_list.append((doc, file))
    return raw_data_list


# Function generates url link to ind file in Bitbucket repo  and appends it to tuple.
def gen_url(file_list, base_url):
    url_list = []
    for f in file_list:
        dir = os.path.dirname(f[1])
        path = f[1].split('/parsers')
        ind_url = base_url + path[1]
        url_list.append((f[0], ind_url, dir))
    return url_list



# block_out's are dicts or strings.
def yaml_out(block):
    return yaml.load(block)


# Function collects all parser sections in a single file and returns them in a list.
def collect_parsers(blocks_list):
    parser_list = []  # handling case with multiple #! PARSER sections
    for b in blocks_list:
        if b.startswith("PARSER"):
            parser_list.append(b)
    return parser_list


# Function to parse AWK and collect metrics in a list.
def parse_awk(parser):
    metrics_list = []
    re_inner_list = re.findall('write[^(]+Metric[^(]*\(.*?\".*?\"', parser)
    for r in re_inner_list:
        metrics = r.split('(')[1].strip('"')
        metrics_list.append(metrics)
    return metrics_list


# Function to parse XML or JSON and collect metrics in a list.
def parse_xml_json(parser):
    metrics_list = []
    re_inner_list = re.findall('im.name"*:*\s*_constant:\s*\".*?\"', parser)
    for r in re_inner_list:
        metrics = r.split('_constant: ')[-1].strip('"')
        metrics_list.append(metrics)
    return metrics_list


# Helper function for traversing the list of files with #! PARSER sections
#  and finding the ones that have #! COMMENTS section.
def find_comments(bloks_list):
    comm_block = []
    for b in bloks_list:
        if b.startswith('COMMENTS'):
            comm_block = b
    return comm_block


'''
Traversing the list of files with #! PARSER sections and finding the ones that have #! COMMENTS section.
Combining meta and comments dicts in tuples and saving it in a list.
Checking if the meta type is "interrogation".
Adding parser lists to previous tuples and saving it the same tuple.
Blocks with YAML formatting or parsing errors are separated to another list.
'''
def pack_dicts(blocks_list):
    packed_dicts = []
    error_blocks = []
    for b in blocks_list:
        dir = b[2]
        meta_block = b[0]
        try:
            meta_dict = yaml_out(meta_block)
        except (yaml.parser.ParserError, yaml.scanner.ScannerError) as yaml_err:
            error_blocks.append((b[0], 'METADATA', yaml_err, b[1]))
            continue

        if "comments" in meta_dict:
            comm_dict = meta_dict["comments"]
        else:
            comm_dict = {}

        if "steps" in meta_dict:
            steps = meta_dict["steps"]
        else:
            error_blocks.append((b[0], 'STEPS', yaml_err, b[1]))
            continue

        metrics_list = []
        for step in steps:
            if "parse" in step:
                parser = step["parse"]
                parse_type = parser["type"]
                parse_file = dir + "/" + parser["file"]
                with open(parse_file, "r") as f:
                    parser_text = f.read()
                    if parse_type == "AWK":
                        metrics_inner_list = parse_awk(parser_text)
                    if  parse_type == "XML" or parse_type == "JSON":
                        metrics_inner_list = parse_xml_json(parser_text)
                    metrics_list.extend(metrics_inner_list)
        if meta_dict['type'] != 'interrogation':
            packed_dicts.append((meta_dict, comm_dict, metrics_list, b[1]))

    return packed_dicts, error_blocks


'''
Function to separate COMMENTS blocks with multiple sections to single tuples.
Dealing with the tuples that have no comments section.
Adding parser lists to previous tuples and saving it in outer tuple.
'''
def to_single(dicts):
    expanded_dicts = []
    notes_dicts = []
    parser_dicts = []
    for t in dicts:
        comm_dict = t[1]
        meta_dict = t[0]
        parser_list = t[2]
        expanded_dicts_inner = []
        if type(comm_dict) is dict:
            if comm_dict:
                for key in comm_dict:
                    expanded_dict = {key: comm_dict[key]}
                    column_dict = {'metric': key}
                    column_dict['note'] = None
                    expanded_dicts_inner.append((meta_dict.copy(), expanded_dict.copy(), column_dict.copy()))
                expanded_dicts.append((expanded_dicts_inner, parser_list, t[3]))
            else:
                parser_dicts.append((meta_dict.copy(), {}, {}, parser_list, t[3]))
        else:
            column_dict = {'metric': 'Error: comment contains note only'}
            notes_dicts.append(
                (meta_dict.copy(), comm_dict, column_dict.copy(), parser_list, t[3]))  # here type of comm_dict is str.
    return expanded_dicts, notes_dicts, parser_dicts


# Function to track metrics found in files without #! COMMENTS sections.
def track_metrics_parsers(dicts):
    parser_metrics_dicts = []
    comm_dict_missing = {key: 'Error: missing field' for key in
                         ('why', 'how', 'without-indeni', 'can-with-snmp', 'can-with-syslog')}
    col_dict_missing = {'note': None, 'all_fields': 'Error: missing field'}
    for d in dicts:
        for m in d[3]:
            # make new row with missing fields
            meta_dict = d[0]
            comm_dict = comm_dict_missing
            col_dict = col_dict_missing
            col_dict['metric'] = m
            parser_metrics_dicts.append(((meta_dict.copy(), comm_dict.copy(), col_dict.copy()), d[4]))
    return parser_metrics_dicts


#todo: consider depreciation of this and notes processing blocks, no more notes seen for a while
# Function for processing the case when a note and all the comments field values
#  are in the same comment section.
def process_notes(dicts):
    proc_notes_dicts = []
    proc_comm_dicts = []
    for d in dicts:
        meta_dict = d[0]
        comm_dict = d[1]
        column_dict = d[2]
        parser_list = d[3]
        ind = comm_dict.find('\n')
        if ind != -1:
            comm_note = comm_dict[:ind]
            comm_block = comm_dict[ind + 1:]
            proc_comm_dict = yaml.load(comm_block)
            for key in proc_comm_dict:
                expanded_dict = {key: proc_comm_dict[key]}
                column_dict['metric'] = key
                column_dict['note'] = comm_note
                proc_comm_dicts.append(((meta_dict.copy(), expanded_dict.copy(), column_dict.copy()), parser_list, d[4]))
        else:
            empty_comm_dict = {}
            for key in ('why', 'how', 'without-indeni', 'can-with-snmp', 'can-with-syslog'):
                empty_comm_dict[key] = 'covered-elsewhere note'
            column_dict['all_fields'] = False
            column_dict['note'] = comm_dict
            proc_notes_dicts.append(((meta_dict.copy(), empty_comm_dict.copy(), column_dict.copy()), parser_list, d[4]))
    return proc_notes_dicts, proc_comm_dicts

# Checking if all the keys are in the comments dictionary.
def check_key_set(dicts, keyset):
    for d in dicts:
        for e in d[0]:  # iterating through expanded dicts inner list of tuples.
            comm_dict = e[1]
            for key in comm_dict:
                fields_dict = comm_dict[key]
                if fields_dict:
                    try:
                        if keyset <= set(fields_dict):
                            e[2]['all_fields'] = True
                        else:
                            if 'skip-documentation' in fields_dict:
                                if fields_dict['skip-documentation']:
                                    e[2]['all_fields'] = 'skip'
                            else:
                                e[2]['all_fields'] = 'Error: Missing fields'
                    except TypeError:
                        pass
                else:
                    comm_dict[key] = {}
                    e[2]['all_fields'] = 'Error: Missing fields'
    return dicts


# Checking if the metrics extracted from the parser sections are covered in the comments section and
# have all the fields.
def check_metrics(dicts):
    missing_metrics_dicts = []
    comm_dict_missing = {key: 'Error: missing field' for key in
                         ('why', 'how', 'without-indeni', 'can-with-snmp', 'can-with-syslog')}
    col_dict_missing = {'note': None, 'all_fields': 'Error: missing field'}
    for d in dicts:
        metrics_comm_list = []
        for e in d[0]:
            metrics_comm = e[2]['metric']
            metrics_comm_list.append(metrics_comm)
        for m in d[1]:
            if m not in metrics_comm_list:
                # make new row with missing fields
                meta_dict = d[0][0][0]
                comm_dict = comm_dict_missing
                col_dict = col_dict_missing
                col_dict['metric'] = m
                missing_metrics_dicts.append(((meta_dict.copy(), comm_dict.copy(), col_dict.copy()), d[2]))
    return missing_metrics_dicts


# Checking if the metrics in the comments section have
#  field 'skip-documetation': True .(It comes out capitalized from the yaml parser).
def filter_skip(flat_dicts):
    filtered_dicts = []
    skip_dicts = []
    for d in flat_dicts:
        if 'all_fields' in d[0][2]:
            if d[0][2]['all_fields'] != 'skip':
                filtered_dicts.append(d)
            else:
                skip_dicts.append(d)
    return filtered_dicts, skip_dicts


# Filters vendors  and os that are marked with 'neq' and ignores these lines.
def filter_neq(flat_dicts):
    filtered_dicts = []
    skip_dicts = []
    for d in flat_dicts:
        if 'vendor' in d[0][0]['requires'] and 'neq' in d[0][0]['requires']['vendor']:
            skip_dicts.append(d)
        elif 'os.name' in d[0][0]['requires'] and 'neq' in d[0][0]['requires']['os.name']:
            skip_dicts.append(d)
        else:
            filtered_dicts.append(d)
    return filtered_dicts, skip_dicts


# Sets vendor as 'linux' if ind script name starts with 'linux' or 'unix'.
def set_vendor_linux(dicts):
    for d in dicts:
        if d[0][0]['name'].startswith(('linux', 'unix')):
            d[0][0]['requires']['vendor'] = 'linux'
    return dicts


# Function maps and prints out name of the block in which YAML formatting error is located,
# file name from #META data and error messages from the error_blocks list.
def print_errors(blocks):
    for b in blocks:
        meta_block = b[0][1]
        meta_name = meta_block.split('\n')[1]
        print('YAML Formating Error in :', b[1])
        print(meta_name)
        print('Error_msg: ', b[2])


# Removing escaped characters '\"' and '\n'
def final_format_parsers(dicts):
    for d in dicts:
        for key in ('why', 'how', 'without-indeni'):
            try:
                d[key] = d[key].replace('\"', '"')
                d[key] = d[key].replace('\\"', '"')
                d[key] = d[key].replace('\n', ' ')
                d[key] = d[key].replace('\\n', ' ')
            except KeyError:
                pass
    return dicts
