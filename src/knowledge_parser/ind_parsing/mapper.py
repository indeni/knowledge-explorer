import os
import datetime as dt
import csv
import shutil

def handleOrAndMapping(orDict, osField):

    finalOsField = osField
    for d in range(len(orDict)):

        if 'os.name' in orDict[d]:
                    if len(finalOsField) > 0:
                        finalOsField += ","

                    finalOsField += (orDict[d]['os.name'])

        if 'or' in orDict[d]:
            return handleOrAndMapping(orDict[d]['or'], finalOsField)

        if 'and' in orDict[d]:
            return handleOrAndMapping(orDict[d]['and'], finalOsField)

    if len(finalOsField) == 0:
        return False
    else :
        return finalOsField

# Functions to map the extracted data to final dictionaries and to
# export them to csv file.

# Final dicts parsing function:
def final_map(dicts):
    final_dicts = []
    for d in dicts:
        # Setting values from meta dict.
        final_dict = {}
        for key in ('name', 'monitoring_interval'):
            if key in d[0][0]:
                final_dict[key] = d[0][0][key]
            else:
                final_dict[key] = False
        for key in ('vendor', 'chassis', 'product'):
            if key in d[0][0]['requires']:
                final_dict[key] = d[0][0]['requires'][key]
            else:
                final_dict[key] = False

        if 'os.name' in d[0][0]['requires']:
            final_dict['os.name'] = d[0][0]['requires']['os.name']
        else:
            if 'or' in d[0][0]['requires']:
                final_dict['os.name'] = handleOrAndMapping(d[0][0]['requires']['or'], "")
            elif 'and' in d[0][0]['requires']:
                final_dict['os.name'] = handleOrAndMapping(d[0][0]['requires']['and'], "")
            else:
                final_dict['os.name'] = 'all'

        # Setting values from comments dict.
        if len(d[0][1]) == 1:
            for key in d[0][1]:
                fields_dict = d[0][1][key]
                for field in ('why', 'how', 'without-indeni', 'can-with-snmp', 'can-with-syslog'):
                    try:
                        if field in fields_dict:
                            final_dict[field] = fields_dict[field]
                        else:
                            final_dict[field] = 'Error: missing field'
                    except TypeError:
                        pass
        else:
            for key in d[0][1]:
                final_dict[key] = d[0][1][key]
        # Setting values from column dict.
        for key in d[0][2]:
            final_dict[key] = d[0][2][key]
        final_dict['ind_url'] = d[1]
        final_dicts.append(final_dict)
    return final_dicts


# Function for the output to to csv.
# Output filename contains the date of writing in the name.
def to_csv(output_dir, fieldnames, dicts):
    output_date = dt.datetime.today().strftime("%Y%m%d")
    outfile = os.path.join(output_dir, output_date + '_ind_output.csv')
    if os.path.exists(output_dir):
        shutil.rmtree(output_dir)
    os.makedirs(os.path.dirname(output_dir))
    with open(outfile, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for d in dicts:
            writer.writerow(d)
    return outfile
