Python package for parsing for .ind files and rules in .scala files in the knowledge directory and
joined result table and web updates outfile generation.

Version: 2.3
Developer: Tomas Kazemekas
Email: tomaskazemekas@gmail.com
Date: 2018 01 05


### Changelog from version 2.2

1. Updated Fortinet matching.
2. Updated remediation parsing with new lines present.


### About

The package contains three Python scripts that perform three main operations:

1. ind_parser.py

The parser is checking if the knowledge directory in repo is newer than the local one, in case it is,
 downloading a set knowledge directory from a repository, finds the files with the set ending,
('.ind' by default), scans them for presence of a set section (#! PARSER by default), processes, translates
 YAML format to dictionaries and checks if all the required filed are filled.

The script also extracts the metrics from the #! PARSER sections depending on the kind of parser (AWK, XML or JSON).
Then the script checks if the extracted metrics are covered in the #! COMMENTS section with all the required fields.
If some metrics is not present in the #! COMMENTS section or the file does not have #! COMMENTS section
a new row with the metrics is added to the output csv file.

The script takes into account that some metrics have field: skip-documentation set to True.
They are already covered in certain files and will not be added to the output file repeatedly.

2. rule_parser.py

The script is parsing rules in .scala files and template based rules. It extracts the data by the following
categories: 'rule_file', 'rule_name', 'metric', 'alert_headline', 'alert_severity','alert_description', 'vendor',
 'alert_remediation'. The lines are separated by single metrics and by vendor. Vendors are identified first
  by alert  headline category and afterwards by extracted vendor names from conditional remediation.

Special formatting characters like '%0.f2' are replaced with letters, escaped characters unescaped.

The data is recorded in .csv file.

Separate template based rules are are parsed by the script.

The script is also parsing data plane pool usage rules. The basic data is extracted from these
 rules and basic data is added from DataplanePoolUsageRule.

3. joiner.py

The script gets the latest ind and rules csv files and produces joined  csv table
performing left outer join on rules with ind data. The column used for join is 'metric'.
The script then does vendor matching in rules and ind files. If vendor column contains OS_ name
the ind scripts are matched on os column. Vendors indicated as 'all' or 'other'
are included as well.

The script drops duplicate rows and writes out the results to csv files.

The script updates Google Sheets file with the results of the joined final output file.

The script generates web output csv file for Indeni alerts page updates.


### Set up

The parser was developed to work with Python version 3.5 or later.

To install the required packages navigate to the project directory and
type on the command line:

pip3 install -r requirements.txt

*(In case install errors are reported, alternative install command that works is:
pip3 install pyyaml requests pandas
)

In case Python 3 is the default version om your machine:

pip install -r requirements.txt

Remember to use the matching version of python and pip to install and run the script: if
you use 'pip3' to install, use 'python3' to run as well.

If you have used the script for ind files or rules parsing before and have installed the required Python
libraries just install additional 'pandas' package:

pip3 install  pandas


### Settings

The settings are set in the 'settings.py' file n the main project directory.

# Git branch setting

From version 0.9 there is a setting to select which git branch to use for download from
the knowledge directory. It can be done by setting constant variable BRANCH
in 'settings.py':
BRANCH = 'staging'  # Set the git branch name (in quotes) to download , e.g., 'master', 'staging'.

# Google Sheets file update setting

The file name for Google Sheets can be set in the settings.py file as GSHEETS_FILE constant
variable.

For the script to be able to update the set Google Sheets file, the file must be shared from
the Google Sheets (Google Drive) file settings. From there in the Share dialog enter
this email:
joined-parsers-updater@joined-parser.iam.gserviceaccount.com

Ensure that this account has write access to the shared document.

After the update it is recommended to check the range setting in the Google Sheets
'Alert by Vendor/OS' worksheet. It does not always adapt to the bigger number of rows in
the updated 'Alert Export' worksheet.

Default setting for Google Sheets updating is True. To disable updating set GSHEETS_UPDATE
constant variable to False.


### Running the scripts

The scripts can be run from the command line from the main project directory 'joined_parsers/'.
The scripts can be run one after the other by entering the commands:

1. To parse the ind files, run:

python3 ind_parser.py

In case Python 3 the default version om your machine:
python ind_parser.py

2. To parse the rule scala files, run:
python3 rule_parser.py

3. To get the joined results table, run:
python3 joiner.py

### The Output

The script saves data to to csv. files in the 'data/output/<branch>/' directory.

The branch directory is created automatically and its name is the same as the
branch name, for example, 'data/output/master.

1. ind files parsing results are saved in directory:
'data/output/<branch>/ind/'

2. rule scala files  parsing results are saved in directory:
'data/output/<branch>/rules/'

3. joined results table is saved in directory:
'data/output/<branch>/joined/'

Joined data output is writen in file with the date of writing in the name e.g.
 '20170320_metric_output.csv'

This file contains metric column and is intended for results correctness
check. File contains only unique rows.

Output sorted by 'vendor' and by 'os' is saved in '_sorted_output.csv'

Final output is written in file, e. g.
 '20170320_final_output.csv'

This file has metrics column removed.


### Filtering

Joined data output can be filtered by vendor and by os. The name of the vendor and os
can be set in the settings.py file.

To get filtered output run joiner.py file as usual.

FILTERING = True  # Set to True to produce filtered output csv files.
FILTER_VENDOR = 'cisco'  # Set vendor name from the vendor column, e.g. 'checkpoint'.
FILTER_OS = 'nxos'  # Set os name from the os column, e.g. 'nxos'.
Set to 'all' if no filtering by os is required.

The filtered output is saved in
'data/output/<branch>/filtered/' directory.

Selected vendor and os name is included as part of the file name.


### Web csv output file generation

The script generates web output csv file optimized for Indeni alerts web pages updating.
The web output file can be found in
'data/output/<branch>/web/' directory.

Smaller test file with 20 rows for web updates is also generated and saved in the web output dir.

The results in the web output file should be extracted from the master directory.

The results are filtered to Indeni officially supported devices.

Addtional fields required for Word Press Advanced Custom Fields are generated.

Titles and Categories are also generated.

Url links to .ind scripts in Bitbucket Indeni Knowledge repo are added.


### Final Status Check

1. ind parsing

Reporting of the final metrics numbers

The total number of extracted metrics, number of skipped metrics, total number of missing metrics and
the number of files with YAML formatting error are printed out at the end of the script. Note that in case there are
 some items in the error files list are printed out after the final metrics numbers.

The printout of the error files list can is set by ERROR_PRINTOUT in the 'settings.py' .

2. rules files parsing

The script checks if there are any unprocessed rules at the end of the parsing and saves the
record of unprocessed information in 'unproc_lst' list. The entry also marks what kind of category was not
extracted, e. g. ('headlines', rule_data_list). The unprocessed single rules form the .scala files are
printed out on the command line with the unprocessed category.

Successful parsing status is also printed out on the command line.

3. Joining of the results

Successful joining status is printed out on the command line.


### Additional notes on parsing logic and implementation

1. ind files parsing

The scripts also handles metrics from the files that have notes in the !# COMMENTS section.
The list with these metrics is called IGNORED_METRICS and can be updated
in the settings part of the script at 'settings.py' file in the main project directory.

Vendors that are marked with 'neq', e.g. {'neq': <vendor>}, are ignored.

In some cases the metrics extracted from the #! PARSER sections are already covered elsewhere in the knowledge
directory files. This case is usually indicated with a note in the #! COMMENTS section. The script takes
into account such cases and track these kind of metrics in IGNORED_METRICS list. The list can be updated
by adding certain metrics manually or copying the values from the 'ignored_metrics_updated' list that is generated
at the end of the run of the script.

Blocks from the files with YAML formatting or parsing errors are separated to another list.
If the list is not empty, it is printed at the end of the script. The printout contains the file name
and vendor fields from the #! META section and the print formatted Error message and problem mark
from the yaml parser indicating the location of the formatting error.

From version 0.4 linux is set as vendor if ind. script name starts with 'linux'.


