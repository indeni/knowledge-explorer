from .ind_parsing.extractor import *
from .ind_parsing.mapper import final_map, to_csv
from .settings import *
import pandas as pd
import os

# The main script for the knowledge dir ind files parsing.
def ind_parser(knwoledge_directory, knowledge_branch):
    # Traversing the last data dir to find all .ind files
    ind_files = find_files(knwoledge_directory, IND_FILE_ENDING)
    print('Total number of files  with the selected ending: ', len(ind_files))

    # Splitting the target sections into text blocks and collecting
    #  all of them into list.
    raw_data = get_raw_data(ind_files)


    # Base ulr to ind files in Bitbucket repo.
    base_ind_url = BASE_URL + '/src/' + knowledge_branch + '/parsers'


    # Generating url links to ind files in Bitbucket repo.
    url_list = gen_url(raw_data, base_ind_url)


    # Traversing the list of files with #! PARSER sections and finding the ones that have #! COMMENTS section.
    # Blocks with YAML formatting or parsing errors are separated to another list.
    parser_packed_dicts, error_blocks = pack_dicts(url_list)
    print('Document sections parsed to dictionaries.')


    # Separating COMMENTS blocks with multiple sections to single tuples.

    expanded_dicts, notes_dicts, parser_dicts = to_single(parser_packed_dicts)

    # Tracking the metrics found in files without #! COMMENTS sections.
    parser_metrics_dicts = track_metrics_parsers(parser_dicts)


    # Processing the case when a note and all the field values are in the same comment section.
    proc_notes_dicts, proc_comm_dicts = process_notes(notes_dicts)

    # Expanding the the dicts with dicts separated from notes list.
    expanded_dicts.extend(proc_comm_dicts)

    # Checking if all the keys are in the comments dictionary.
    expanded_dicts = check_key_set(expanded_dicts, REQUIRED_KEYS)
    print('Presence of the required keys in the section checked.')


    # Checking if the metrics extracted from the parser sections are covered in the comments section and
    # have all the fields.
    missing_metrics_dicts = check_metrics(expanded_dicts)

    # Flatening the checked dicts.
    checked_dicts = [(d[0], d[2]) for d in expanded_dicts]
    flat_checked_dicts = [(t, d[1]) for d in checked_dicts for t in d[0]]

    #flat_checked_dicts = [t for d in checked_dicts for t in d]


    # Checking if the metrics in the comments section have
    #  field 'skip-documentation': True .(It comes out capitalized from the yaml parser).
    # filtered_dicts, skip_dicts = filter_skip(flat_checked_dicts)
    filtered_dicts = flat_checked_dicts
    skip_dicts = []

    extended_filtered_dicts = filtered_dicts[:]

    # Adding the dicts from the missing metrics lists
    extended_filtered_dicts.extend(missing_metrics_dicts)

    # Adding the dicts with the metrics from the files without !# COMMENTS section.
    extended_filtered_dicts.extend(parser_metrics_dicts)

    extended_neq_filtered_dicts, neq_skipped_dicts = filter_neq(extended_filtered_dicts)

    linux_set_dicts = set_vendor_linux(extended_neq_filtered_dicts)

    skip_dicts.extend(neq_skipped_dicts)

    final_dicts_unformat = final_map(linux_set_dicts)

    final_dicts = final_format_parsers(final_dicts_unformat)

    print('Final mapping of the dictionaries completed.')
    print('Total number of extracted metrics: ', len(final_dicts) + len(skip_dicts))
    print('Number of skipped documentation metrics: ', len(skip_dicts))
    print('Total number of tracked metrics: ', len(final_dicts))
    print('Number of metrics with all the fields in comments: ', len(filtered_dicts))
    total_missing_metrics = (len(missing_metrics_dicts) +
                             len(parser_metrics_dicts))
    print('Total number of missing metrics: ', total_missing_metrics)


    # Writing the final output to csv.
    ind_output_dir = BASE_OUTPUT_DIR + knowledge_branch + '/ind/'

    outfile = to_csv(ind_output_dir, IND_FIELDNAMES, final_dicts)

    # Removing the "note" column from the ind output
    f=pd.read_csv(outfile)
    keep_col = ['name','monitoring_interval','vendor','os.name', 'chassis', 'product', 'metric', 'why', 'how', 'without-indeni', 'can-with-snmp', 'can-with-syslog', 'all_fields', 'ind_url']
    new_f = f[keep_col]
    new_f.to_csv("newFile.csv", index=False)
    toName = "" + outfile
    os.remove(outfile)
    os.rename("newFile.csv", toName)

    print('The extracted data saved in the output csv file:', outfile)

    print('Number of files with YAML formatting errors: ', len(error_blocks))
    # Checking if there are any error blocs and printing them out.
    if error_blocks and ERROR_PRINTOUT:
        print('Printing out the error files list')
        print_errors(error_blocks)
        print('The script finished as expected with ERROR_PRINTOUT = True.')
        print('Final parsing output numbers are displayed above the error files list printout.')
