from knowledge_parser.ind_parser import ind_parser
from knowledge_parser.rule_parser import rule_parser
from knowledge_parser.joiner import joiner
from knowledge_generator.listVendors import listVendors
from knowledge_generator.generateRST import generateRst
from knowledge_parser.generate_rule_documentation \
    import rule_documentation_builder, \
    save_rule_documentation_as_rst_for_generation, \
    save_rule_documentation_as_json
from arg_parser import ArgParser
import os
import subprocess
import sys
import requests
import zipfile
import shutil
import io
import cgi



# Method that runs the Indeni parser, the Rules parser, and the Joiner script
# This returns the most up-to-date CSV file
def parse_knowledge_src(knowlage_src_dir, knowledge_branch):
    ind_parser(knowlage_src_dir, knowledge_branch)
    rule_parser(knowlage_src_dir, knowledge_branch)
    merge_knowledge_csv_file = joiner(knowledge_branch)
    return merge_knowledge_csv_file


# Method that takes Knowledge data from Joiner files and produces RST, and then HTML, files
# Places HTML files into path 'src/data/generated_knowledge_documentation'
def generateKnowledgeDocumentation(finalJSON, knowledgeDBFile):
    # Runs ListVendors to list all known vendors, so we can organize documentation by vendor
    listVendors(knowledgeDBFile)

    # Generates RST, using the JSON created from knowledgeFile
    generateRst(finalJSON)

    # Runs command line code to prepare for Sphinx build, then builds with Sphinx tool
    run_cli('cp knowledge_generator/conf.py ./data/generate_rst/conf.py')
    run_cli('mkdir -p ./data/generate_rst/images')
    run_cli('cp knowledge_generator/Indeni_Logo.png ./data/generate_rst/images')
    run_cli('sphinx-build -b html ./data/generate_rst/ ./data/generated_knowledge_documentation/')


# Helper method that allows command line code to run in this Python script
# Allows us to run the Sphinx build command straight from this code
def run_cli(cmd, run_dir=None, sudoPass=None, exit_on_failure=True, verbose=True):
    current_dir = os.getcwd()
    if run_dir:
        os.chdir(run_dir)
    if sudoPass:
        cmd = 'echo {0} | sudo -S {1}'.format(sudoPass, cmd)
    print("working dir: " + os.getcwd())

    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    console_output = ''
    while True:
        line = p.stdout.readline()
        if line == '' and p.poll() != None:
            break
        if verbose == True:
            sys.stdout.write(line)
            sys.stdout.flush()
            console_output += line

    exitCode = p.returncode
    if (exitCode == 0):
        if not os.path.exists(current_dir):
            os.makedirs(current_dir)
        os.chdir(current_dir)
        return console_output
    else:
        os.chdir(current_dir)
        if exit_on_failure == True:
            raise Exception



# Function checks if the file in the repo is newer than in the data dir.
#  If it is newer, downloads and unzips the knowledge dir.
def checked_download_zip(download_dir, url):
    if os.path.exists(download_dir):
        shutil.rmtree(download_dir)
    os.makedirs(os.path.dirname(download_dir))
    head = requests.head(url)
    cont_disp = head.headers['content-disposition']
    value, params = cgi.parse_header(cont_disp)
    dest_filename = params['filename']
    dest_dir = dest_filename.split('.')[0]
    if dest_dir in os.listdir(download_dir):
        last_data_dir = download_dir + dest_dir
        print("The latest knowledge directory is already downloaded.")
    else:
        r = requests.get(url)
        z = zipfile.ZipFile(io.BytesIO(r.content))
        z.extractall(path=download_dir)
        last_data_dir = download_dir + z.namelist()[0]  # get the name of the latest downloaded dir
        print("Downloaded newer version of knowledge directory from the repo.")
    return last_data_dir


def download_knowledge(branch_name):
    BASE_URL = 'https://bitbucket.org/indeni/indeni-knowledge'
    BASE_DOWNLOAD_DIR = './data/downloads/'

    # Single rules in.scala files parsing.
    # Downloading the knowledge dir.
    url = BASE_URL + '/get/' + branch_name + '.zip'

    print('Starting download from the branch: ', branch_name)

    download_dir = BASE_DOWNLOAD_DIR + branch_name + '/'

    last_knowledge_dir = checked_download_zip(download_dir, url)

    return last_knowledge_dir

# Running main method, which calls the methods that generate knowledge documentation from most up-to-date knowledge
if __name__ == '__main__':

    args = vars(ArgParser().parse_args())

    knowledge_branch = args['knowledge_branch']
    knowledge_version = args['build_version']
    knowledge_directory = args['knowledge_dir_path']

    if args['full_documentation'] == 'True':
        print('Generating full knowledge documentation')
        knowledgeDBFile = parse_knowledge_src(knowledge_directory,knowledge_branch)

        # Converting CSV file into more organized JSON file, to streamline Rule parsing
        rules_documentation = rule_documentation_builder(knowledgeDBFile)
        rules_documentation_rst_format = save_rule_documentation_as_rst_for_generation(rules_documentation)

        # Using the CSV file from Joiner to produce HTML documentation output
        generateKnowledgeDocumentation(rules_documentation_rst_format, knowledgeDBFile)

    elif args['knowledge_db'] == 'True':
        print('Generating knowledge data base')
        knowledgeDBFile = parse_knowledge_src(knowledge_directory, knowledge_branch)
        print('Creating JSON DB file')
        # Converting CSV file into more organized JSON file, to streamline Rule parsing
        rules_documentation = rule_documentation_builder(knowledgeDBFile)
        knowledge_db_json_file_path = save_rule_documentation_as_json(rules_documentation, knowledge_version)

        print ('knowledge db file created in ' + knowledge_db_json_file_path)

    elif args['knowledge_db_from_local'] == 'True':
        print('Generating knowledge data base from local csv copy')
        # Converting CSV file into more organized JSON file, to streamline Rule parsing
        rules_documentation = rule_documentation_builder("data/output/master/joined/20180814_sorted_output.csv")
        knowledge_db_json_file_path = save_rule_documentation_as_json(rules_documentation)