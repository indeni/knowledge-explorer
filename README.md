# knowledge-explorer

##install

`pip install -r requirements.txt`

##Generate indeni-knowledge-HTML-documentation

The indeni knowledge explorer used in order to auto generate the `indeni-knowloedge-documentation` in HTML. 

To create the full indeni-knowledge HTMl documentation run `python generate_documentation.py --full-documentaion` this will download the indeni-knowledge `master` branch, run the `knowledge-joiner` to generate knowledge DB in a CSV format. Than it will use the CSV file to RST files, which each RST page is then converted to HTML using the Sphinx tool for creating HTML from RST files. Once the python code finish you can browse to `/src/data/generated_knowledge_documentation/index.html`

To specify different branch you can simply pass `--knowledge-branch <your-branch>` as argument


##Generate indeni-knowledge-DB

To generate a JSON "DB" of indeni-knowledge run `python generate_documentation.py --knowledge-db True`. This will create a json file that contains indeni-knowledge DB in `src/data/output/knowledge.db.json`

Example of rule documentation object:

```json
{
        "vendor": "bluecoat", 
        "os": "sgos", 
        "rule_combine_title": "cross_vendor_network_port_down-bluecoat-sgos", 
        "rule_combine_alert_headline": "Network port(s) down-bluecoat-sgos", 
        "metrics": [
            {
                "metric_name": "network-interface-admin-state", 
                "metric_description": [
                    {
                        "how": "This script logs into the Bluecoat Proxy through SSH and retrieves the output of the \"show interface all\" command. The output includes the admin state of the network interfaces. ", 
                        "ind_url": "https://bitbucket.org/indeni/indeni-knowledge/src/master/parsers/src/bluecoat/proxysg/show-interface-all.ind", 
                        "why": "An administrator might set a network interface to be disabled for troubleshooting, but should he he forget about doing this network trunks might be running at reduced capacity. ", 
                        "script_name": "bluecoat-show-interface-all", 
                        "without_indeni": "An administrator could login to the device through SSH and manually issue the command \"show interface all\". "
                    }
                ]
            }
        ], 
        "rule_name": "cross_vendor_network_port_down", 
        "alert_remediation": "Review the cause for the ports being down.", 
        "rule_file": "PortIsDownRule.scala", 
        "alert_description": "Indeni will alert if one or more network ports is down.", 
        "alert_headline": "Network port(s) down"
    }
```  
